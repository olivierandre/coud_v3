'use strict'

class Init
	constructor: ->
		new Click($('#showMenuExist'))
		.showMenuExist()

		new Click($('.imageForArticle'))
		.getImageForArticle()

class Click
	constructor: (@selector) ->

	getImageForArticle: ->
		@selector.on 'click', ->
			img = $(this).find 'img'
			new TextArea()
			.addImageIntoTextarea(img)

	showMenuExist: ->
		@selector.on 'click', ->
			console.log 'ok'

class TextArea
	constructor: () ->

	addImageIntoTextarea: (image) ->
		image = image[0]
		width = 960
		height = 640

		if image.width < image.height
			width = 640
			height = 960

		figure = '
		<div class="col-xs-12">
		<figure class="figureArticle">
			<img class="img-responsive center-block" title="' + image.title + '" alt="' + image.alt + '" height="' + height + '" width="' + width + '" src="' + image.src + '"/>
			<figcaption>' + image.title + '</figcaption>
		</figure></div>'

		tinyMCE.execCommand('mceInsertContent', false, figure);


$ ->
	init = new Init()