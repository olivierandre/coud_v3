var Guid = (function() {
    'use strict';

    var self = {};

    var s4 = function() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    };

    self.getGuid = function() {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    };

    return self;
}());


var Direction = (function(guid) {
    var self = {},
        tab = [],
        timeoutId;

    self.init = function(className) {
        self.className = className;
    };

    self.detectPosition = function(element, pageY, pageX, top, left, height, width) {
        var child = element.parentNode.querySelector('.' + self.className);
        var bandeau = element.parentNode.querySelector('.bandeauImageSquare');
        var text = bandeau.querySelector('.mobileTitleImage').innerHTML;
        if (child) {
            element.parentNode.removeChild(child);
        }
        child = createDiv(width, height, text);

        //  http://stackoverflow.com/questions/3627042/jquery-animation-for-a-hover-with-mouse-direction/3647634#3647634
        var x = (pageX - left - (width / 2)) * (width > height ? (height / width) : 1),
            y = (pageY - top - (height / 2)) * (height > width ? (width / height) : 1),
            index = Math.round((((Math.atan2(y, x) * (180 / Math.PI)) + 180) / 90) + 3) % 4,
            end = 0;

        if (element.getAttribute('id') === null) {
            element.setAttribute('id', guid.getGuid());
        }

        tab[element.id] = {
            index: index,
            width: width,
            height: height
        };

        switch (index) {
            case 0:
                child.style.top = '-' + height + 'px';
                element.parentNode.appendChild(child);
                timeoutId = setTimeout(function() {
                    child.style.top = end;
                }, 100);

                break;
            case 1:
                child.style.right = '-' + width + 'px';
                element.parentNode.appendChild(child);
                timeoutId = setTimeout(function() {
                    child.style.right = end;
                    child.style.top = end;
                }, 100);
                break;
            case 2:
                child.style.bottom = '-' + height + 'px';
                element.parentNode.appendChild(child);
                timeoutId = setTimeout(function() {
                    child.style.bottom = end;
                }, 100);
                break;
            case 3:
                child.style.left = '-' + width + 'px';
                element.parentNode.appendChild(child);
                timeoutId = setTimeout(function() {
                    child.style.left = end;
                    child.style.top = end;
                }, 100);
                break;
            default:
        }
        bandeau.style.height = 0;
    };

    var createDiv = function(width, height, text) {
        var div = document.createElement('div');
        div.style.width = width + 'px';
        div.style.height = height + 'px';
        //div.style.backgroundColor = 'rgba(0, 0, 0, 0.7)';
        div.style.position = 'absolute';
        div.style.padding = '10px';
        div.style.cursor = 'pointer';
        div.className = self.className + ' showImage';

        var border = document.createElement('div');
        border.style.width = '100%';
        border.style.boxSizing = 'border-box';
        border.style.height = '100%';
        //border.style.border = '5px solid white';
        border.style.textAlign = 'right';
        border.className = 'transitionBorder';

        var paragraph = document.createElement('p');
        paragraph.innerHTML = 'Agrandir l\'image';
        paragraph.style.color = 'rgb(255, 255, 255)';
        paragraph.style.position = 'relative';
        paragraph.style.top = '10px';
        paragraph.style.fontSize = '20px';
        paragraph.style.fontStyle = 'italic';
        paragraph.style.right = '20px';

        var paragraphText = document.createElement('p');
        paragraphText.innerHTML = text;
        paragraphText.style.color = 'rgb(255, 255, 255)';
        paragraphText.style.position = 'absolute';
        paragraphText.style.bottom = '10px';
        paragraphText.style.fontSize = '20px';
        paragraphText.style.fontStyle = 'italic';
        paragraphText.style.left = '20px';

        border.appendChild(paragraph);
        border.appendChild(paragraphText);
        div.appendChild(border);

        setTimeout(function() {
            border.style.opacity = 1;
        }, 200);
        return div;
    };

    self.removeContainer = function(element) {
        var parent = element.parentNode,
            animate = parent.querySelector('.' + self.className),
            obj = tab[element.id],
            bandeau = parent.querySelector('.bandeauImageSquare'),
            transitionBorder = parent.querySelector('.transitionBorder');
        clearTimeout(timeoutId);

        switch (obj.index) {
            case 0:
                animate.style.top = '-' + obj.height + 'px';
                break;
            case 1:
                animate.style.right = '-' + obj.width + 'px';
                break;
            case 2:
                animate.style.bottom = '-' + obj.height + 'px';
                break;
            case 3:
                animate.style.left = '-' + obj.width + 'px';
                break;
        }

        bandeau.style.height = '50px';
        transitionBorder.style.opacity = 0;
        setTimeout(function() {
            resetContainer(animate);
            if (transitionBorder.parentNode !== undefined && transitionBorder.parentNode !== null)
                transitionBorder.parentNode.removeChild(transitionBorder);
        }, 500);
    };

    var resetContainer = function(element) {
        element.style.top = '';
        element.style.bottom = '';
        element.style.left = '';
        element.style.right = '';
        element.style.width = '';
        element.style.height = '';
    };

    return self;

}(Guid));

var Square = (function(direction) {
    var self = {};

    self.init = function(className, container) {
        if (className !== undefined && container !== undefined && className.trim().length > 0 && container.trim().length > 0) {
            direction.init(className);
            var containers = document.getElementsByClassName(container);
            if (containers.length > 0) {
                setTimeout(function() {
                    Array.prototype.forEach.call(containers, function(container) {
                        var parent = container.parentNode;
                        parent.style.height = container.height + 'px';

                        window.addEventListener('resize', function() {
                            parent.style.height = container.height + 'px';
                        });

                        container.addEventListener('mouseenter', function(e) {
                            var rect = this.getBoundingClientRect(),
                                bodyElt = document.body,
                                height = rect.height,
                                width = rect.width,
                                top = rect.top + (document.documentElement.scrollTop || document.body.scrollTop),
                                left = rect.left + (document.documentElement.scrollLeft || document.body.scrollLeft);

                            direction.detectPosition(this, e.pageY, e.pageX, top, left, height, width);

                        });

                        parent.addEventListener('mouseleave', function(e) {
                            direction.removeContainer(container);
                        });
                    });

                }, 400);

            }

        }

    };

    return self;
}(Direction));
