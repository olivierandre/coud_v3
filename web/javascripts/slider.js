var Slider = (function () {
	'use strict';

	var self = {};
	var container,
		slider,
		wrappers,
		len,
		size = 0,
		left = 0,
		visible = 0,
		clickRight,
		clickLeft,
		hasPress = false,
		images = [],
		imageWidth;


	self.init = function () {

		container = document.getElementById('container-slider');
		slider = document.getElementById('slider');
		wrappers = slider.getElementsByClassName('wrapper');
		len = wrappers.length;
		clickRight = slider.querySelector('.right');
		clickLeft = slider.querySelector('.left');

		Array.prototype.forEach.call(wrappers, function (wrapper, index) {
			var image = wrapper.querySelector('.aImage');
			imageWidth = image.clientWidth;

			container.style.width = imageWidth + 'px';
			wrapper.style.left = -(size) + 'px';
			wrapper.style.width = imageWidth + 'px';

			var idx = parseInt(wrapper.dataset.idx);

			var transform = imageWidth;
			if (idx === 0) {
				transform = 0;
				wrapper.style.visibility = 'visible';
			} else if (idx === 1) {
				wrapper.style.visibility = 'visible';
			} else if (idx === len - 1) {
				transform = 0 - imageWidth;
				wrapper.style.visibility = 'visible';
			} else {
				wrapper.style.visibility = 'hidden';
			}

			wrapper.style.transform = 'translate(' + transform + 'px, 0px) translateZ(0px)';
			size += image.clientWidth;
			images.push(image);

		});
		slider.style.width = size + 'px';

		clickRight.addEventListener('click', slideRight);
		clickLeft.addEventListener('click', slideLeft);

		window.addEventListener('keydown', function (key) {
			if (!hasPress) {
				hasPress = true;
				switch (key.keyCode) {
					case 37:
						slideLeft();
						break;
					case 39:
						slideRight();
						break;
					default:

				}
				setTimeout(function () {
					hasPress = false;
				}, 400);
			}
		});

		window.addEventListener('resize', doResize);
		setTimeout(doResize);
        setTimeout(doResize);

        slideImageInterval(5000);

	};

    var slideImageInterval = function(duration) {
        self.intervalSlide = setInterval(function() {
            slideRight();
        }, duration);
    };

    var clearSlideInterval = function() {
        clearInterval(self.intervalSlide);
        slideImageInterval(5000);
    };

	var slideRight = function () {
		var nextVisible;
		clickRight.style.pointerEvents = 'none';
		Array.prototype.forEach.call(wrappers, function (wrapper, index) {
			var idx = parseInt(wrapper.dataset.idx),
				image = wrapper.querySelector('.aImage'),
				imageWidth = image.clientWidth,
				transform = null,
				futurVisible = visible + 1 > len - 1 ? 0 : visible + 1,
				futurAfter = futurVisible + 1 > len - 1 ? 0 : futurVisible + 1,
				futurBefore = visible - 1 < 0 ? 0 : visible - 1;
			wrapper.style.visibility = 'hidden';

			// En cours
			if (idx === visible) {
				transform = -(imageWidth);
				wrapper.style.transform = 'translate(' + transform + 'px, 0px) translateZ(0px)';
				wrapper.style.visibility = 'visible';
			} else if (idx === futurVisible) {
				transform = 0;
				wrapper.style.transform = 'translate(' + transform + 'px, 0px) translateZ(0px)';
				wrapper.style.visibility = 'visible';
				nextVisible = idx;
			} else if (idx === futurAfter) {
				transform = imageWidth;
				wrapper.style.transform = 'translate(' + transform + 'px, 0px) translateZ(0px)';
			} else if (idx === futurBefore) {
				transform = imageWidth;
				setTimeout(function () {
					wrapper.style.transform = 'translate(' + transform + 'px, 0px) translateZ(0px)';
				}, 400);
			}

			if (idx === len - 1) {
				setTimeout(function () {
					clickRight.style.pointerEvents = 'auto';
				}, 400);
			}
		});
		visible = nextVisible;

        clearSlideInterval();
	};

	var slideLeft = function () {
		var nextVisible;
		clickLeft.style.pointerEvents = 'none';
		Array.prototype.forEach.call(wrappers, function (wrapper, index) {
			var idx = parseInt(wrapper.dataset.idx),
				image = wrapper.querySelector('.aImage'),
				imageWidth = image.clientWidth,
				transform = null,
				futurVisible = visible - 1 < 0 ? len - 1 : visible - 1,
				futurAfter = futurVisible - 1 < 0 ? len - 1 : futurVisible - 1,
				futurBefore = visible - 1 < 0 ? 0 : visible - 1;


			wrapper.style.visibility = 'hidden';

			// En cours
			if (idx === visible) {
				transform = imageWidth;
				wrapper.style.transform = 'translate(' + transform + 'px, 0px) translateZ(0px)';
				wrapper.style.visibility = 'visible';
			} else if (idx === futurVisible) {
				transform = 0;
				wrapper.style.transform = 'translate(' + transform + 'px, 0px) translateZ(0px)';
				wrapper.style.visibility = 'visible';
				nextVisible = idx;
			} else if (idx === futurAfter) {
				transform = imageWidth;
				wrapper.style.transform = 'translate(' + -(transform) + 'px, 0px) translateZ(0px)';
			} else if (idx === futurBefore) {
				transform = imageWidth;
				setTimeout(function () {
					wrapper.style.transform = 'translate(' + transform + 'px, 0px) translateZ(0px)';
				}, 400);

			}

			if (idx === len - 1) {
				setTimeout(function () {
					clickLeft.style.pointerEvents = 'auto';
				}, 400);

			}
		});
		visible = nextVisible;
        clearSlideInterval();
	};

	var doResize = function () {
		var ratio = 1,
			newSize, halfNewSize,
			marginLeft;

		var left = document.getElementById('left'),
			right = document.getElementById('right'),
            sliderHeight = document.getElementById('slider').getBoundingClientRect().height,
            importantTitleHeight = document.querySelector('.importantTitle').getBoundingClientRect().height,
			screenSize = document.documentElement.clientWidth; // Bug IOS 9

		if (screenSize < imageWidth) {
			ratio = screenSize / imageWidth;
			container.style.marginLeft = '-15px';
		} else {
			container.style.marginLeft = '';
		}

		newSize = imageWidth * ratio;
		halfNewSize = (newSize / 2) + 'px';

		container.style.width = newSize + 'px';
		left.style.width = halfNewSize;
        left.style.height = sliderHeight - importantTitleHeight + 'px';
        right.style.height = sliderHeight - importantTitleHeight + 'px';
		right.style.width = halfNewSize;
		right.style.left = halfNewSize;

		var index = 0,
			len = images.length;

		for (index; index < len; index++) {
			images[index].style.width = newSize + 'px';
		}
	};

	return self;
}());
