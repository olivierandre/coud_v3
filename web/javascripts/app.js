var Ajax = (function() {
  'use strict';
  var self = {};

  self.init = function(formName, buttonToClick) {
    self.buttonToClick = buttonToClick;
    self.form = document.forms[formName];
    self.formName = formName;
    self.submitButton = document.getElementById(self.formName + '_' + 'submit');
    hadEventOnForm(formName);
  };

  var defineHttpRequest = function() {
    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
      return new XMLHttpRequest();
    } else if (window.ActiveXObject) { // IE
      return new ActiveXObject("Microsoft.XMLHTTP");
    }
  };

  var ajaxSend = function(form, callback) {
    var httpRequest = defineHttpRequest();
    var formData = new FormData(form);

    httpRequest.onreadystatechange = function() {
      if (httpRequest.status === 200 && httpRequest.readyState == 4) {
        var response = JSON.parse(httpRequest.responseText);
        callback(response);
      }
    };

    httpRequest.open('POST', form.action, true);
    httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    httpRequest.send(formData);

  };

  self.ajaxSendViaUrl = function(url, callback) {
    var httpRequest = defineHttpRequest();

    httpRequest.onreadystatechange = function() {
      if (httpRequest.status === 200 && httpRequest.readyState == 4) {
        var response = JSON.parse(httpRequest.responseText);
        callback(response);
      }
    };

    httpRequest.open('POST', url, true);
    httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    httpRequest.send();

  };

  var hadEventOnForm = function(formName) {
    self.form.addEventListener('submit', function(e) {
      self.submitButton.disabled = true;
      ajaxSend(this, styleErrors);
      e.preventDefault();
    });
  };

  self.resetClass = function() {
    var formGroup = document.getElementsByClassName('form-group'),
      len = formGroup.length,
      index = 0;

    for (index; index < len; index++) {
      formGroup[index].classList.remove('has-error');
      var span = formGroup[index].getElementsByTagName('span');
      if (span.length > 0) {
        formGroup[index].removeChild(span[0]);
      }
    }
  };

  var showAlert = function(typeAlert, text) {
    var alert = document.getElementById('alert');
    alert.classList.add(typeAlert);
    alert.innerHTML = text;
    alert.style.display = 'block';
    setTimeout(function() {
      alert.style.opacity = 1;
    }, 400);

    setTimeout(function() {
      alert.style.opacity = 0;
      setTimeout(function() {
        alert.style.display = 'none';
        self.buttonToClick.click();
      }, 650);

    }, 3000);
  };

  var styleErrors = function(response) {
    self.resetClass();
    if (!response.result) {
      var errors = $.map(response.errors, function(value, index) {
        var element = document.getElementById(self.formName + '_' + index);
        createHtmlError(element.parentNode, value);
        if (response.response !== undefined) {
          //showAlert('alert-danger', response.response);
          toastr.error(response.response);
        }

      });
    } else if (response.result === 1) {
      //showAlert('alert-success', response.response);
      toastr.success(response.response);
      self.form.reset();
    } else if (response.result === 2) {
      //showAlert('alert-danger', response.response);
      toastr.error(response.response);
    }
    self.submitButton.disabled = false;
  };


  var createHtmlError = function(element, text) {
    var span = document.createElement('span');
    span.setAttribute('class', 'help-block');

    var ul = document.createElement('ul');
    ul.setAttribute('class', 'list-unstyled');

    var li = document.createElement('li');

    var spanGlyph = document.createElement('span');
    spanGlyph.setAttribute('class', 'glyphicon glyphicon-exclamation-sign');

    li.appendChild(spanGlyph);
    li.innerHTML += ' ' + text;

    ul.appendChild(li);
    span.appendChild(ul);
    element.appendChild(span);
    element.classList.add('has-error');
  };

  return self;
}());

var Comment = (function(ajax) {
  'use strict';
  var self = {};

  self.init = function(formName) {
    var commentShowForm = document.getElementById('commentShowForm');
    var commentShowButton = document.getElementById('commentShowButton');
    commentShowForm.addEventListener('click', function() {
      var form = document.getElementById('commentForm');
      var display = form.style.display;
      if (display === 'none') {
        form.style.display = 'block';
        this.innerHTML = 'Cacher le formulaire';
      } else {
        document.forms[formName].reset();
        ajax.resetClass();
        form.style.display = 'none';
        this.innerHTML = 'Effectuer un commentaire';
      }
    });

    if (commentShowButton !== null) {
      var count = document.getElementById('countComments').innerHTML;
      commentShowButton.innerHTML = 'Afficher les commentaires (' + count + ')';
      commentShowButton.addEventListener('click', function() {
        var allComments = document.getElementById('allComments');
        var display = allComments.style.display;
        if (display === 'none') {
          allComments.style.display = 'block';
          this.innerHTML = 'Cacher les commentaires';
        } else {
          allComments.style.display = 'none';
          this.innerHTML = 'Afficher les commentaires (' + count + ')';
        }
      });
    }

  };

  return self;

}(Ajax));

var NavBar = (function() {
  'use strict';

  var self = {},
    defaultSize = 1000,
    navbar = document.getElementById('navbar'),
    hasEvent = false;

  self.init = function() {
    window.addEventListener('resize', addOrRemoveClass);
    addOrRemoveClass();
  };

  var openDesktopMenu = function() {
    if (navbar.classList.contains('hide-menu')) {
      navbar.classList.add('show-menu');
      navbar.classList.remove('hide-menu');
      this.classList.add('close-menu');
      setTimeout(function() {
        navbar.style.zIndex = 0;
      }, 400);
    } else {
      navbar.classList.remove('show-menu');
      navbar.classList.add('hide-menu');
      this.classList.remove('close-menu');
      navbar.style.zIndex = -1;

    }
  }

  var addOrRemoveClass = function() {
    if (navbar) {
      var screenWidth = document.documentElement.clientWidth;

      if (screenWidth < defaultSize) {
        navbar.classList.remove('pull-right');
        navbar.classList.remove('show-menu');
        navbar.classList.add('hide-menu');
      } else {
        navbar.classList.add('pull-right');
        if (!hasEvent) {
          hasEvent = true;
          var openDesktopMenuElement = $('#open-desktop-menu');
          openDesktopMenuElement.on('click', openDesktopMenu);
        }

      }
    }
  };


  return self;
}());

var Css = (function() {
  'use strict';

  var self = {};

  self.createFullDiv = function(zindex, bgColor) {
    var div = document.createElement('div');
    div.style.width = '100%';
    div.style.height = '100%';
    div.style.position = 'absolute';
    div.style.top = 0;
    div.style.left = 0;
    div.style.zIndex = zindex;
    div.style.backgroundColor = bgColor;
    div.style.opacity = 0.7;

    return div;
  };

  self.createModal = function(width, height, zindex, isPixel) {
    var px = '';
    if (isPixel === undefined || isPixel) {
      px = 'px';
    }
    var modal = document.createElement('div');
    modal.className = 'row';
    modal.style.width = width + px;
    //modal.style.height = height + 'px';
    modal.style.position = 'fixed';
    modal.style.backgroundColor = '#fff';
    modal.style.zIndex = zindex;
    modal.style.padding = '0 20px 20px';
    modal.style.left = ((window.innerWidth - width) / 2) + 'px';
    modal.style.top = '50%';
    modal.style.textAlign = 'center';
    window.addEventListener('resize', function() {
      modal.style.left = ((window.innerWidth - width) / 2) + 'px';
      //modal.style.top = ((window.innerHeight - height) / 2) + 'px';
    });

    return modal;
  };

  self.createModalFixed = function(text) {
    var modal = self.createModal('100%', '100%', 0, false);
    modal.innerHTML += text;
    modal.style.padding = '15px';
    modal.style.top = '0px';
    modal.className = 'animeTop';
    modal.style.position = 'fixed';
    modal.style.color = '#fff';

    return modal;
  };


  return self;

}());

var Message = (function(toastr) {
  'use strict';
  var self = {};

  self.setMessage = function(type, message, title) {
    if (!title)
      title = null;

    switch (type) {
      case 'success':
        toastr.success(message, title);
        break;
      case 'error':
        toastr.error(message, title);
        break;
      default:

    }
  };

  return self;

}(toastr));

var Photo = (function(ajax, css) {
  'use strict';

  var self = {};

  var eventResize = function() {
    window.addEventListener('resize', resizeImage);
  };

  var resizeImage = function() {
    var height = window.innerHeight,
      width = window.innerWidth,
      image = document.getElementById('imgFadePhoto'),
      heightImage = image.clientHeight,
      widthImage = image.clientWidth;

    var style = marginPhoto(width, height, widthImage, heightImage);
    image.style.marginTop = style.marginTop + 'px';
    image.style.marginLeft = style.marginLeft + 'px';

    var paragraph = image.nextElementSibling;
    var left = marginParagraph(width, widthImage);
    paragraph.style.left = left.left + 'px';
  };

  self.fadePhotoSquare = function(url, title) {
    var height = window.innerHeight,
      width = window.innerWidth;
    var image = new Image();
    image.setAttribute('src', url);
    image.setAttribute('class', 'image img-responsive');
    image.setAttribute('id', 'imgFadePhoto');

    var paragraph = document.createElement('p');
    paragraph.setAttribute('class', 'imageTitle');
    paragraph.innerHTML = title;

    var div = document.createElement('div');
    div.setAttribute('id', 'fadePhoto');
    div.setAttribute('class', 'fadePhotoDiv');
    div.style.opacity = 0;
    div.addEventListener('click', function(e) {
      blur(false);
      div.style.opacity = 0;
      setTimeout(function() {
        this.parentNode.removeChild(div);
        window.removeEventListener('resize', resizeImage);
      }.bind(this), 1000);
      e.preventDefault();
    });

    setTimeout(function() {
      var heightImage = image.clientHeight,
        widthImage = image.clientWidth;
      var style = marginPhoto(width, height, widthImage, heightImage);
      image.style.marginTop = style.marginTop + 'px';
      image.style.marginLeft = style.marginLeft + 'px';
      var left = marginParagraph(width, widthImage);
      paragraph.style.left = left.left + 'px';
      div.style.opacity = 1;
      blur(true);
    }, 400);

    div.appendChild(image);
    div.appendChild(paragraph);
    document.body.appendChild(div);

    eventResize();

  };

  var blur = function(isBlur) {
    var container = document.getElementById('container'),
      navbarTop = document.getElementById('navbarTop'),
      blur = 'blur(5px)',
      none = 'none';
    container.style.transition = '-webkit-filter: 1s';

    if (isBlur) {
      navbarTop.style.webkitFilter = blur;
      container.style.webkitFilter = blur;
    } else {
      container.style.webkitFilter = none;
      navbarTop.style.webkitFilter = none;
    }

  };

  var marginPhoto = function(width, height, widthImage, heightImage) {
    return {
      marginTop: height > heightImage ? (height - heightImage) / 2 : 0,
      marginLeft: width > widthImage ? (width - widthImage) / 2 : 0
    };
  };

  var marginParagraph = function(width, widthImage) {
    return {
      left: width > widthImage ? (width - widthImage) / 2 + 30 : 30
    };
  };

  self.deleteImage = function(url, id) {
    self.div = css.createFullDiv(1050, '#000');
    self.modal = css.createModal(400, 200, 1100);
    var text = '<div class="col-xs-12"><p style="margin-top: 20px;">Voulez-vous supprimer définitivement cette image?</p></div><div class="col-xs-6"><button class="btn btn-success" onclick="Photo.sendAjaxRequest()">Oui</button></div><div class="col-xs-6"><button onclick="Photo.removeDivAndModal()" class="btn btn-default">Non</button></div>';
    self.modal.innerHTML += text;
    document.body.appendChild(self.div);
    document.body.appendChild(self.modal);

    self.div.addEventListener('click', self.removeDivAndModal);
    self.toDelete = document.getElementById('row_' + id);
    self.url = url;
    return false;
  };

  self.sendAjaxRequest = function() {
    ajax.ajaxSendViaUrl(self.url, Photo.removeElement);
  };

  self.removeDivAndModal = function() {
    document.body.removeChild(self.div);
    document.body.removeChild(self.modal);
  };

  self.removeElement = function(result) {
    var modal = css.createModalFixed(result.response);

    if (result.result === 1) {
      self.toDelete.parentNode.removeChild(self.toDelete);
      delete self.toDelete;
      modal.style.backgroundColor = '#dff0d8';
      modal.style.color = '#3c763d';

    } else if (!result.result) {
      modal.style.backgroundColor = '#f2dede';
      modal.style.color = '#a94442';
    }

    self.removeDivAndModal();
    document.body.appendChild(modal);
    setTimeout(function() {
      modal.style.top = '63px';
    }, 400);
    setTimeout(function() {
      modal.style.top = 0;
    }, 4000);
    setTimeout(function() {
      document.body.removeChild(modal);
    }, 4500);

  };

  return self;

}(Ajax, Css));

//  https://robinosborne.co.uk/2016/05/16/lazy-loading-images-dont-rely-on-javascript/
var LazyLoad = (function(Slider) {
  'use strict';
  var self = {},
    lazy,
    carrousel;

  self.init = function() {
    lazy = document.getElementsByClassName('lazy');
    carrousel = document.getElementById('carrousel');
    lazyLoad();
    lazyLoadContainer();
    window.addEventListener('scroll', lazyLoad);
    window.addEventListener('scroll', lazyLoadContainer);
  }

  var lazyLoadContainer = function() {
    if (carrousel && isInViewport(carrousel)) {
      var promise = $.get('/api/getImportantsDocuments');
      promise.then(function(html) {
        var slider = $('#section_slider');
        return $.Deferred().resolve(slider.append($(html)));
      }).then(function() {
        setTimeout(function() {
          Slider.init();
        }, 400);
      });
      carrousel = null;
    }
  }

  var lazyLoad = function() {
    var i = 0,
      len = lazy.length;

    for (; i < len; i++) {
      var element = lazy[i];
      if (element && isInViewport(element)) {
        element.src = element.getAttribute('data-src');
        element.srcset = element.getAttribute('data-srcset');
        element.removeAttribute('data-src');
        element.removeAttribute('data-srcset');
      }
    }
    cleanLazy();
  }

  var cleanLazy = function() {
    lazy = Array.prototype.filter.call(lazy, function(l) {
      return l.getAttribute('data-src');
    });
  }

  var isInViewport = function(el) {
    var rect = el.getBoundingClientRect();
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
      rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
  };

  return self;
})(Slider);

window.addEventListener('DOMContentLoaded', function() {
  if (("standalone" in window.navigator) && window.navigator.standalone) {
    var links = document.getElementsByTagName('a');
    Array.prototype.forEach.call(links, function(link) {
      if (!link.classList.contains('noStandalone')) {
        link.addEventListener('click', function(e) {
          e.preventDefault();
          location.href = this.getAttribute('href');
        });
      }

    });
  }
  NavBar.init();
  LazyLoad.init(Slider);
});
