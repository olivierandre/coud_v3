var SocialNetwork = (function ($) {
	'use strict';

	var self = {};

	self.init = function (route, title) {
		$.ajaxSetup({
			cache: true
		});
		var promise = $.getScript('//connect.facebook.net/fr_FR/sdk.js');
		promise.then(function () {
			FB.init({
				appId: '979895058799142',
				version: 'v2.8',
				cookie: true,
				status: true,
				xfbml: true,
			});
			FB.AppEvents.logPageView();
		});

		var facebookShare = $('#facebookShare'),
			twitterShare = $('#twitterShare');

		facebookShare.on('click', function () {
			FB.ui({
				method: 'share',
				href: route
			}, function (response) {
				FB.AppEvents.logEvent("shareArticle");
			});
		});

		twitterShare.on('click', function () {
			var url = 'https://twitter.com/intent/tweet?text=' + title + '&url=' + route;
			window.open(url, '_blank');
		});
	};

	return self;

}(jQuery));
