var SizeImage = (function ($) {
	'use strict';

	var self = {};

	self.init = function () {
		var buttons = document.getElementsByClassName('delete-size'),
			index = 0;

		for (; index < buttons.length; index++) {
			buttons[index].addEventListener('click', addEvent);
		}
	};

	var addEvent = function () {
		var id = this.dataset.id,
			parent = 'image-type-' + id;
		var promise = $.ajax({
			url: 'supprimer-taille-image/' + id,
			method: 'POST'
		});

		managePromise(promise).done(function (response) {
			toastr.success(response);
			var element = document.getElementById(parent);
			element.parentNode.removeChild(element);
		}.bind(this)).fail(function (value) {
			toastr.warning(response);
		});

	};

	var managePromise = function (promise) {
		var deferred = $.Deferred();
		promise.done(function (result) {
			if (result.result === 1) {
				deferred.resolve(result.response);
			} else {
				deferred.reject(result.response);
			}
		});
		return deferred;

	};

	return self;

}(jQuery));
