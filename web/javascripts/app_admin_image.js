$(document).on('change', '.btn-file :file', function () {
	$("#oldImage").remove();
	var input = $(this),
		numFiles = input.get(0).files ? input.get(0).files.length : 1,
		label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
});

var imageLoader = document.getElementById('image_file');
imageLoader.addEventListener('change', handleImage, false);
var canvas = document.getElementById('imageCanvas');
var ctx = canvas.getContext('2d');

function handleImage(e) {
	var reader = new FileReader();
	reader.onload = function (event) {
		var img = new Image();
		img.onload = function () {
			canvas.width = img.width;
			canvas.height = img.height;
			ctx.drawImage(img, 0, 0);
		};
		img.src = event.target.result;
	};
	reader.readAsDataURL(e.target.files[0]);
}

$(document).ready(function () {
	$('.btn-file :file').on('fileselect', function (event, numFiles, label) {
		var input = $(this).parents('.input-group').find(':text'),
			log = numFiles > 1 ? numFiles + ' files selected' : label;

		if (input.length) {
			input.val(log);

		} else {
			if (log) alert(log);
		}

	});
});
