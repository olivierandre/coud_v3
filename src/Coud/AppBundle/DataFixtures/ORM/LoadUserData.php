<?php 
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Coud\AppBundle\Entity\User;
use Coud\AppBundle\Entity\Role;
use Coud\AppBundle\Entity\MainMenu;
use Coud\AppBundle\Entity\SubMenu;
use Coud\AppBundle\Entity\Article;
use Coud\AppBundle\Entity\Image;
use Coud\AppBundle\Entity\ImageSize;
use Coud\AppBundle\Entity\Aside;
use Coud\AppBundle\Util\StringHelper;
  
class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $slug = $this->container->get('slugify');

        // Création des rôles
        $roleSuperAdmin = new Role();
        $roleAdmin = new Role();
        $roleUser = new Role();
        $roleAuthor = new Role();

        $roleSuperAdmin->setName('Super adminstrateur');
        $roleSuperAdmin->setRole('ROLE_SUPER_ADMIN');
        $roleSuperAdmin->setPriority(0);
        $roleAdmin->setName('Administrateur');
        $roleAdmin->setRole('ROLE_ADMIN');
        $roleAdmin->setPriority(1);
        $roleAuthor->setName('Auteur');
        $roleAuthor->setRole('ROLE_AUTHOR');
        $roleAuthor->setPriority(2);
        $roleUser->setName('Utilisateur');
        $roleUser->setRole('ROLE_USER');
        $roleUser->setPriority(3);

        $manager->persist($roleSuperAdmin);
        $manager->persist($roleAdmin);
        $manager->persist($roleAuthor);
        $manager->persist($roleUser);
        

        // Création de l'utilisateur
        $userSuperAdmin = new User();
        $userSuperAdmin->setUsername('oandre');
        $userSuperAdmin->setEmail('olivier.andre77@gmail.com');
        $userSuperAdmin->setFirstName('Olivier');
        $userSuperAdmin->setLastName('Andre');
        $userSuperAdmin->setActive(true);

        $userAdmin= new User();
        $userAdmin->setUsername('jkaluszynski');
        $userAdmin->setEmail('judith.kalus@gmail.com');
        $userAdmin->setFirstName('Judith');
        $userAdmin->setLastName('Kaluszynski');
        $userAdmin->setActive(true);

        // Gestion du salt
        $stringHelper = new StringHelper();
        $userSuperAdmin->setSalt($stringHelper->getSaltToken());
        $userAdmin->setSalt($stringHelper->getSaltToken());

        // Gestion du token
        $userSuperAdmin->setToken($stringHelper->getSaltToken());
        $userAdmin->setToken($stringHelper->getSaltToken());

        // Crypté les passwords - On le met en 'dure' (à modifier par la suite)
        $factory = $this->container->get('security.encoder_factory');
        $encoder = $factory->getEncoder($userSuperAdmin);
        $password = $encoder->encodePassword('1234', $userSuperAdmin->getSalt());
        $userSuperAdmin->setPassword($password);

        $encoder = $factory->getEncoder($userAdmin);
        $password = $encoder->encodePassword('1234', $userAdmin->getSalt());
        $userAdmin->setPassword($password);

        // On indique le rôle 
        $userSuperAdmin->addRole($roleSuperAdmin);
        $userAdmin->addRole($roleAdmin);
        $manager->persist($userSuperAdmin);
        $manager->persist($userAdmin);
        

        //  Créations des sous menus
        $subArticle = new SubMenu();
        $subArticle->setTitle('Articles');
        $subArticle->setLink($slug->slugify($subArticle->getTitle()));

        $manager->persist($subArticle);
        

        $subMenu = new SubMenu();
        $subMenu->setTitle('Portfolio');
        $subMenu->setLink($slug->slugify($subMenu->getTitle()));

        $manager->persist($subMenu);
        

        //  Création des menus
        $mainMenuCouture = new MainMenu();
        $mainMenuCouture->setTitle('Créations couture');
        $mainMenuCouture->setLink($slug->slugify($mainMenuCouture->getTitle()));
        $mainMenuCouture->setActive(true);
        $mainMenuCouture->addSubMenu($subArticle);
        $mainMenuCouture->addSubMenu($subMenu);

        $manager->persist($mainMenuCouture);
        

        $mainMenu = new MainMenu();
        $mainMenu->setTitle('Créations tricot/crochet');
        $mainMenu->setLink($slug->slugify($mainMenu->getTitle()));
        $mainMenu->setActive(true);
        $mainMenu->addSubMenu($subArticle);
        $mainMenu->addSubMenu($subMenu);

        $manager->persist($mainMenu);
        

        //  Création des tailles d'images
        $imageSize = new ImageSize();
        $imageSize->setTitle('Thème');
        $imageSize->setWidth(288);
        $imageSize->setHeight(192);
        $imageSize->setDescription('Image pour les thèmes des articles');
        $imageSize->setSlug($slug->slugify($imageSize->getTitle()));
        $manager->persist($imageSize);
        

        $imageSizeArticle = new ImageSize();
        $imageSizeArticle->setTitle('Article');
        $imageSizeArticle->setWidth(960);
        $imageSizeArticle->setHeight(640);
        $imageSizeArticle->setDescription('Image pour article et portfolio');
        $imageSizeArticle->setSlug($slug->slugify($imageSizeArticle->getTitle()));
        $manager->persist($imageSizeArticle);
        


        //  Création image par défaut
        $image = new Image();
        $image->setImageSize($imageSize);
        $image->setTitle('Thème par défaut');
        $image->setAlt('Thème par défaut');
        $image->setPath('theme-par-defaut.jpeg');
        $manager->persist($image);
        

        //  Création des articles
        $article = new Article();
        $article->setTitle('La veste Lobélie');
        $article->setContent('<p>L’année dernière, en novembre, je me suis rendue au salon “créations et savoir-faire”. <br>
        C’est un salon qui regroupe tout ce qui se fait sur le thème du tricot, de la couture, du scrapbooking et de la cuisine. <br>
        Apres avoir arpenté les différentes allées, vu la célèbre Mercotte, achetée des magnifiques tissus, je suis tombée sur un stand qui m’a tapé dans l’œil.</p>
        <p>Le stand de Lou&amp;Me, jeune créatrice de patron.  <br>
        J’en suis repartie avec le patron de la veste Lobélie avec l’intention de faire une veste pour chacune de mes 2 filles, mais l’hiver passant, j’avais rangé le patron dans mon placard en revenant du salon.</p>
        <p>Arrive les beaux jours, et j’ai donc ressorti le patron et réfléchi aux associations que je voulais.</p>
        <p>Ma grande voulait une veste en Jean, et on a choisi la doublure en rose Fuchsia. <br>
        Pour la petite, je suis restée sur la veste en jean, mais j’aime beaucoup de jaune Banane de France Duval Stalla.</p>
        <p>J’ai même réussi à trouver les boutons assortis à la doublure !<br>
        La veste a un joli faux col passepoilé et il est doublé avec le tissus Fuchsia étoiles argentées de France Duval Stalla.</p>
        <p>Je la trouve vraiment très bien coupée, et elle est nickel pour ma grande! <br>
        J’ai mis à peu près 3 semaines avant de faire la version pour ma petite, mais je vous la montre prochainement.</p>
       ');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subArticle);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("04/24/2014"));
        $article->setDateModified(new \DateTime("04/24/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('La reine du Smock');
        $article->setContent('<p>Samedi, j’ai eu la chance de pouvoir suivre un cours de Smocks dans l’atelier-boutique de Linna Morata. Notre grande professeur était Karen, la reine du Smocks ! (liberty Addict et Smocks addict).</p>
            <p><br />Etant autodidacte en couture, c’était le premier cours que je suivais et j’ai été agréablement surprise. J’ai eu beaucoup de plaisir à pouvoir échanger avec des femmes que je ne connaissais pas. J’ai souvent l’impression de passer pour une folle quand je parle couture…</p>
            <p>Et en plus, j’ai appris une technique que je ne connaissais pas du tout. Tout le matériel était fourni, on avait plus qu’à sortir notre fil et nos aiguilles ! Et la bonne humeur de Karen nous a grandement facilité la tâche !</p> 
            <p>En 3 heures, on a juste eu le temps de smocker. J’ai donc pris le temps dimanche de finir ma jolie trousse !</p>                     
            <p>Je n’ai qu’une envie, maintenant, c’est de faire des robes smockées à mes filles pour cet été !</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subArticle);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("03/10/2014"));
        $article->setDateModified(new \DateTime("03/10/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('Ma nouvelle machine à coudre');
        $article->setContent('<p>Avant d’acheter une machine à coudre très perfectionnée, j’ai préféré investir dans une petite machine très basique.</p>
            <p>Je voulais être sûr que je m’éclaterai en couture et que ce n’était pas juste une lubie de quelques semaines … J’ai donc pris, en octobre 2012, une petite Singer, à juste 100 euros.</p>
            <p>Aujourd’hui, après 15 mois de bons et loyaux services, il était temps pour moi de me rendre à l’évidence : ma MAC était devenu trop simple. J’avais besoin de nouveaux points, de silence, de rapidité… c’est pourquoi je viens d’investir sur une machine à coudre de compétition !</p> 
            <p>J’ai bien regardé les marques qui pouvaient me plaire et je suis tombée sur les Pfaff. Le design soft me plaisait bien : j’aimais beaucoup le coté carré, et non pas arrondis…</p>
            <p><br/>Les avis disaient tous qu’elles étaient des MAC très solides. Et finalement, mon choix c’est porté sur la Expression 4.2.</p> 
            <p>Aujourd’hui, je sais que je n’utiliserais pas tous les points dans l’immédiat, mais je sais que cette machine fera un bon bout de chemin avec moi, et que peut être, je la donnerais a l’une de mes filles dans quelques années !</p>
            ');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subArticle);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("01/24/2014"));
        $article->setDateModified(new \DateTime("01/24/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('Le crochet');
        $article->setContent('<p>Le crochet, j’y suis tombée en janvier 2013, quand j’ai découvert les patrons de Lalylala. Cette créatrice  fait des trop mignons petits animaux en crochet, et moi, j’avais choisi de faire le Kangourou "Kira", l’ours "Bina" et le renard "Fibi".</p>
        <p>Une fois les patrons achetés, il fallait bien que je m’équipe en laine et en crochet… Je suis allée dans une boutique de laine de ma ville, mais elle m’a pas forcement donné la laine la plus simple a travailler, et j’ai trouvé ca un peu galère a faire…</p>
        <p>Etant fraichement maman de ma 2eme poulette, j’ai laissé de coté les patrons, ma laine et mon crochet…</p>
        <p>Et un jour, en me baladant sur Paris, je suis tombée sur le livre de  Nekoyama qui explique comme créer un chat au crochet ! Donc, j’ai voulu faire un second essai… Mais rien a faire, je n’y arrivait pas. Ca ne crocheter pas trop bien, je devais louper une étape…</p>
        <p>Mais j’ai tendance a ne pas m’avouer vaincu, et les magnifiques créations que je voyais sur le net me laissait encore le crochet dans un coin de la tête…</p>
        <p>J’ai donc trouvé un autre livre qui apprenait à faire le « panier de la marchande au crochet ». Et grâce a ce livre, j’ai enfin compris d’ou venait mon erreur, et j’ai fait un poisson pour ma fille avec la laine qu’il me restait de mes différents essais.</p>
        <p>Et maintenant, une autre reine du crochet a sorti un livre de crochet, et j’ai réussi à faire en moins de 2 semaines la jolie petite Lila ! Prénom choisi par ma grande !!</p>
        <p>Voilà, maintenant, j’ai compris comment ca marche, et je suis équipée en livre et patrons !</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenu);
        $article->setSubMenu($subArticle);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("06/16/2014"));
        $article->setDateModified(new \DateTime("06/16/2014"));

        $manager->persist($article);
        


        //  Création des portfolios
        $article = new Article();
        $article->setTitle('Smock');
        $article->setContent('<p>Trousse smockée, lin beige et doublure bleu canard. fils à smock DMC.</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("02/14/2014"));
        $article->setDateModified(new \DateTime("02/14/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('Saint-Valentin : les Coeurs');
        $article->setContent('<p>Coeurs en tissus. une face Liberty et une face unie.</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("02/14/2014"));
        $article->setDateModified(new \DateTime("02/14/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('Veste en jean - Taille 8 ans');
        $article->setContent('<p>Veste en jean, en taille 8 ans, tirée du patron Lobélie de Lou&me. Tissus Jean, doublure en Batiste Fushia étoiles argentées et boutons assorties France Duval Stalla.</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("03/31/2014"));
        $article->setDateModified(new \DateTime("03/31/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('Guirlande de fanion \'Liberty\' : Mirabelle');
        $article->setContent('<p>Guirlande de 9 Fanions, environ 2,5m. Liberty Mirabelle, plumetis beige, Première étoile taupe et biais Gris.</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("03/31/2014"));
        $article->setDateModified(new \DateTime("03/31/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('Guirlande de fanion : Taupe');
        $article->setContent('<p>Guirlande de 9 Fanions, environ 2,5m. Tissus taupe première étoile et lettre velours lisse.</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("03/31/2014"));
        $article->setDateModified(new \DateTime("03/31/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('Livre Doudou personnalisé');
        $article->setContent('<p>Livre doudou en tissus, avec différentes formes (triangle, lune, rond...) et matières (lainage, plumetis, polaire...).</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("04/13/2014"));
        $article->setDateModified(new \DateTime("04/13/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('Plaid en Liberty & Jersey et son coussin');
        $article->setContent('<p>Plaid avec une face avec plusieurs tissus & motifs et une face en Jersey uni.</p>
                            <p>Tissu Liberty Betsy Anne rose, Jersey figue France Duval Stalla, tissu Chevron vert d\'eau, tissu première étoile.</p>
                            <p>Dimension du plaid : 80cm x 100cm. Dimension de la housse de coussin : 50cm x 50cm</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("05/01/2014"));
        $article->setDateModified(new \DateTime("05/01/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('Valisette en Liberty Betsy platine');
        $article->setContent('<p>Valisette en tissu. Extérieur Liberty Betsy platine. Intérieur coton blanc & étoile argentée.</p><p>Dimension : 20cm x 15cm</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("05/02/2014"));
        $article->setDateModified(new \DateTime("05/02/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('Cabas Paillette');
        $article->setContent('<p>Trousse smockée, lin beige et doublure bleu canard. fils à smock DMC.</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("05/04/2014"));
        $article->setDateModified(new \DateTime("05/04/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('Coussin Liberty Mitsi');
        $article->setContent('<p>Modèle Rose : Liberty Mitsi valeria rose, tissus fuchsia étoilé, passepoil gris pailleté.</p>
                                <p>Modèle vert : Liberty Mitsi menthe, tissus première étoile et passepoil argenté</p>
                                <p>Dimension : 30x50cm</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("05/22/2014"));
        $article->setDateModified(new \DateTime("05/22/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('Vide poche - Cadeau pour la maîtresse');
        $article->setContent('<p>Poche fourre-tout qui sert pour ranger les petites affaires de la salle de bain ou du change des bébés, par exemple.</p>
                                <p>Tissus cahier d\'école et blanc étoiles noires Broderie.</p>
                                <p>Dimension : Hauteur : 19cm - Largeur : 15cm.</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("05/24/2014"));
        $article->setDateModified(new \DateTime("05/24/2014"));

        $manager->persist($article);
        


        $article = new Article();
        $article->setTitle('Sac & besace en bandoulière');
        $article->setContent('<p><u>Sac en bandoulière besace fuchsia et Liberty Betsy rose/rouge</u><br>En simili cuir fuchsia et doublure en Liberty Betsy rouge/rose ayant une poche à l\'intérieur.<br>Le sac fait 36cm par 28 cm avec une bandoulière d\'environ 130 cm et 5 cm de large.<br>Le rabat (à reposer au-dessus du sac et se fermant avec une pression) fait 20cm par 29 cm.<br><br><u>Sac en bandoulière besace turquoise et étoile</u><br>En simili cuir turquoise et doublure en tissus bleu paon et étoiles blanches ayant une poche à l\'intérieur.<br>Le sac fait 36cm par 28 cm avec une bandoulière d\'environ 100 cm et 6 cm de large.<br>Le rabat (à reposer au-dessus du sac) fait 20cm par 29 cm.</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("05/26/2014"));
        $article->setDateModified(new \DateTime("05/26/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('Tunique Smock');
        $article->setContent('<p>Tuniques modèle "Agnés de citronille" en tissu petit pan. Modèle avec smock.</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("06/10/2014"));
        $article->setDateModified(new \DateTime("06/10/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('Sac à goûter pour enfant');
        $article->setContent('<p>Voici le sac à dos "Made In Judith" !!!</p>
                                <p>C\'est un sac à dos enfant, idéal pour le goûter, emmener doudou et le carnet de correspondance pour la maternelle.</p>
                                <p>Il est fait en jean et doublure en Liberty Capel. Il a une poche à l\'avant pour y apporter un paquet de mouchoir. Il se ferme par un rabas et un bouton pression</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("10/11/2014"));
        $article->setDateModified(new \DateTime("10/11/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('Les chaussons en cuir pour les enfants');
        $article->setContent('<p>Paire de chaussons enfant en cuir avec la doublure en tissus Liberty</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("11/16/2014"));
        $article->setDateModified(new \DateTime("11/16/2014"));

        $manager->persist($article);
        

        $article = new Article();
        $article->setTitle('Portefeuille Liberty simili cuir');
        $article->setContent('<p>Portefeuille en simili cuir bleu marine et en tissus thème England. Dimension : 12cm x 20cm. Fermeture par pression. Peut contenir un chéquier, 6 cartes, 1 pièce d\'identité, un permis de conduire, de la monnaie, des tickets de bus ou métro ...</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenuCouture);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("11/30/2014"));
        $article->setDateModified(new \DateTime("11/30/2014"));

        $manager->persist($article);

        $article = new Article();
        $article->setTitle('Bonhomme en crochet');
        $article->setContent('<p>Modèle framboise de tournicote, fil DMC natura de couleur corail et bleu canard.</p>');
        $article->setPublished(true);
        $article->setAuthor($userAdmin);
        $article->setMainMenu($mainMenu);
        $article->setSubMenu($subMenu);
        $article->setImage($image);
        $article->setDateCreated(new \DateTime("06/09/2014"));
        $article->setDateModified(new \DateTime("06/09/2014"));

        $manager->persist($article);
        
        $manager->flush();


           
    }       
}