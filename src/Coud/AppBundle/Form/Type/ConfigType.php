<?php

namespace Coud\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ConfigType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('limitImportant', NumberType::class, array(
                'label' => 'Nombre affichages d\'articles \'important\'',
                'attr' => array(
                    'placeholder' => 'Obligatoire')
                )
            )
            ->add('submit', SubmitType::class, array(
                'label' => 'Valider',
                'attr' => array(
                'class' => 'btn btn-primary'
                )
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Coud\AppBundle\Entity\Config'
        ));
    }
}
