<?php

namespace Coud\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ImageSizeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Titre de la taille d\'image'
                ))
            ->add('width', IntegerType::class, array(
                'label' => 'Longueur'
                ))
            ->add('height', IntegerType::class, array(
                'label' => 'Largeur'
                ))
            ->add('description', TextareaType::class)
            ->add('submit', SubmitType::class, array(
                    'label' => 'Sauvegarder la nouvelle taille',
                    'attr' => array(
                        'class' => 'btn btn-primary'
                        )
                ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Coud\AppBundle\Entity\ImageSize'
        ));
    }
}
