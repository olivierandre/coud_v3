<?php

namespace Coud\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MainMenuType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, array(
                    'label' => 'Nom du menu'
                ))
            ->add('active', null, array(
                    'label' => 'Le rendre actif ?'
                ))
            ->add('subMenu', EntityType::class, array(
                    'class' => 'CoudAppBundle:SubMenu',
                    'multiple' => true,
                    'expanded' => true,
                    'choice_label' => 'title',
                    'label' => 'L\'associé au sous-menu suivant :'
                ))
            ->add('submit', SubmitType::class, array(
                    'label' => 'Créer le menu',
                    'attr' => array(
                        'class' => 'btn btn-primary'
                        )
                ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Coud\AppBundle\Entity\MainMenu'
        ));
    }

}
