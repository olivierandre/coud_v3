<?php

namespace Coud\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CommentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, array(
                'label' => "Votre prénom",
                'label_attr' => array('class' => 'pull-right'),
                'attr' => array(
                    'placeholder' => 'Obligatoire')
                ))
            ->add('lastName', TextType::class, array(
                'label' => "Votre nom",
                'label_attr' => array('class' => 'pull-right'),
                ))
            ->add('email', EmailType::class, array(
                'label' => 'Votre adresse mail',
                'label_attr' => array('class' => 'pull-right'),
                'attr' => array(
                    'placeholder' => 'Obligatoire')
                ))
            ->add('content', TextareaType::class, array(
                'label' => 'Votre commentaire',
                'label_attr' => array('class' => 'pull-right'),
                'attr' => array(
                    'class' => 'tinymce',
                    'placeholder' => 'Obligatoire'
                    )
                ))
            ->add('submit', SubmitType::class, array(
                    'label' => 'Valider',
                    'attr' => array(
                        'class' => 'btn btn-primary'
                        )
                ));
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Coud\AppBundle\Entity\Comment'
        ));
    }

}
