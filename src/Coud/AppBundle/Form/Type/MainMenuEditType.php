<?php

namespace Coud\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class MainMenuEditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('submit')
            ->add('submit', SubmitType::class, array(
                    'label' => 'Modifier le menu',
                    'attr' => array(
                        'class' => 'btn btn-primary'
                        )
                ))
        ;
    }

    public function getParent() {
        return MainMenuType::class;
    }

}
