<?php

namespace Coud\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RoleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $opts = $options['options'];

        if($opts['modify']) {
            $disabled = true;
            $label = "Mise à jour du rôle";
        } else {
            $disabled = false;
            $label = "Création du rôle";
        }

        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nom du nouveau rôle',
                'attr' => array(
                    'placeholder' => "Obligatoire"
                    )
                ))
            ->add('role', TextType::class, array(
                'label' => 'Nom technique',
                'attr' => array(
                    'placeholder' => "Commence toujours par 'ROLE_'",
                    ),
                'disabled' => $disabled
                ))
            ->add('priority', null, array(
                'label' => "Indiquer une priorité",
                'attr' => array(
                    'placeholder' => "Obligatoire"
                    )
                ))
            ->add('submit', SubmitType::class, array(
                    'label' => $label,
                    'attr' => array(
                        'class' => 'btn btn-primary'
                        )
                ));
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Coud\AppBundle\Entity\Role',
            'options' => null
        ));
    }
}
