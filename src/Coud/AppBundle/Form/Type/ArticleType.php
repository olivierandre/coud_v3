<?php

namespace Coud\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ArticleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $opts = $options['options'];

        $builder
            ->add('title', TextType::class, array(
                'label' => 'Titre de l\'article',
                'attr' => array(
                    'placeholder' => 'Obligatoire'
                    )
                ))
            // ->add('typeArticle', 'entity', array(
            //         'class' => 'CoudAppBundle:TypeArticle',
            //         'multiple' => false,
            //         'expanded' => true,
            //         'property' => 'name',
            //         'label' => 'L\'associé au type d\'article suivant :'
            //     ))
            ->add('mainMenu', EntityType::class, array(
                    'class' => 'CoudAppBundle:MainMenu',
                    'multiple' => false,
                    'expanded' => true,
                    'choice_label' => 'title',
                    'label' => 'L\'associé au menu suivant :'
                ))
            ->add('subMenu', EntityType::class, array(
                    'class' => 'CoudAppBundle:SubMenu',
                    'multiple' => false,
                    'expanded' => true,
                    'choice_label' => 'title',
                    'label' => 'L\'associé au sous-menu suivant :'
                ))
            ->add('image', EntityType::class, array(
                    'class' => 'CoudAppBundle:Image',
                    'query_builder' => function(\Coud\AppBundle\Entity\ImageRepository $imageRepository) use ($opts) {
                            $modify = false;

                            if(!empty($opts['modify'])) {
                                $modify = $opts['modify'];
                            }

                            $qb = $imageRepository->createQueryBuilder('i')
                                ->leftjoin('i.imageSize', 's')
                                ->leftJoin('i.imageArticle', 'a')
                                ->where('s.slug = :slug')
                                ->andWhere('a.image is null')
                                ->setParameter(':slug', $opts['imageSlug']);
                            //  Exclut les images d'articles déjà utilisées

                            if($modify) {
                                $qb->orWhere('a.image = :image')
                                ->setParameter(':image', $opts['imageTheme']);
                            }
                            return $qb;
                    },
                    'multiple' => false,
                    'expanded' => true,
                    'choice_label' => 'title',
                    'label' => 'Choisir une image pour le théme',
                ))
            ->add('content', TextareaType::class, array(
                'label' => 'Contenu de l\'article',
                'attr' => array(
                    'class' => 'tinymce',
                    'placeholder' => 'Obligatoire'
                    )
                ))
            ->add('submit', SubmitType::class, array(
                    'label' => 'Etape suivante',
                    'attr' => array(
                        'class' => 'btn btn-primary'
                        )
                ));

            if($opts['portfolio']) {
                $builder->add('images', EntityType::class, array(
                    'label' => 'Choisir les images pour le slide',
                    'class' => 'CoudAppBundle:Image',
                    'choice_label' => 'title',
                    'multiple' => true,
                    'expanded' => true,
                    'query_builder' => function(\Coud\AppBundle\Entity\ImageRepository $imageRepository) use ($opts) {
                            $modify = false;

                            if(!empty($opts['modify'])) {
                                $modify = $opts['modify'];
                            }

                            $qb = $imageRepository->createQueryBuilder('i')
                            ->leftjoin('i.imageSize', 's')
                            ->leftJoin('i.imageArticle', 'a')
                            ->where('s.slug = :slug')
                            ->andWhere('a.image is null')
                            ->setParameter(':slug', $opts['document'])
                            ->orderBy('i.dateCreated', 'DESC');
                            //  Exclut les images d'articles déjà utilisées

                            if($modify) {
                                // $qb->orWhere('a.image = :image')
                                // ->setParameter(':image', $opts['imageTheme']);
                            }
                            return $qb;
                    }
                    ));
            }

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Coud\AppBundle\Entity\Article',
            'options' => null
        ));
    }

}
