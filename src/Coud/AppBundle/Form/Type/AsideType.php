<?php

namespace Coud\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class AsideType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Titre du "Qui-suis-je ?"'
                ))
            ->add('content', TextareaType::class, array(
                'label' => 'Contenu de l\'article',
                'attr' => array(
                    'class' => 'tinymce',
                    'placeholder' => 'Obligatoire'
                    )
                ))
            ->add('file', FileType::class, array(
                'label' => 'Choix du fichier ...'
                ))
            ->add('submit', SubmitType::class, array(
                    'label' => $options['label'],
                    'attr' => array(
                        'class' => 'btn btn-primary'
                        )
                ));
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Coud\AppBundle\Entity\Aside',
            'label' => null
        ));
    }

}
