<?php

namespace Coud\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SubMenuType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, array(
                    'label' => 'Nom du menu'
                ))
            // ->add('mainMenu', 'entity', array(
            //         'class' => 'CoudAppBundle:MainMenu',
            //         'multiple' => false,
            //         'property' => 'title',
            //         'label' => 'L\'associé au menu suivant :'
            //     ))
            ->add('submit', SubmitType::class, array(
                    'label' => 'Créer le sous-menu',
                    'attr' => array(
                        'class' => 'btn btn-primary'
                        )
                ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Coud\AppBundle\Entity\SubMenu'
        ));
    }
}
