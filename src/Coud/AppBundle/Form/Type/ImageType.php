<?php

namespace Coud\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ImageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, array(
                'label' => 'Titre de l\'image',
                'attr' => array(
                    'placeholder' => 'Obligatoire'
                    )
                ))
            ->add('alt', null, array(
                'label' => 'Texte alternatif',
                'attr' => array(
                    'placeholder' => 'Obligatoire'
                    )))
            ->add('imageSize', EntityType::class, array(
                'class' => 'CoudAppBundle:ImageSize',
                'expanded' => true,
                'multiple' => false

                ))
            ->add('file',  null, array(
                'label' => 'Choix du fichier ...',
                'attr' => array(
                    'id' => 'yo'
                )
            ))
            ->add('submit', SubmitType::class, array(
                    'label' => 'Sauvegarder l\'image',
                    'attr' => array(
                        'class' => 'btn btn-primary'
                        )
                ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Coud\AppBundle\Entity\Image',
            'csrf_protection' => false,
        ));
    }
}
