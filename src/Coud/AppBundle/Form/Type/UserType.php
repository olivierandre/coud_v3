<?php

namespace Coud\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $opts = $options['options'];
        $admin = false;
        $disabled = true;
        $reinit = false;
        $create = false;

        if(!empty($opts['admin'])) {
            $admin = $opts['admin'];

            if(!empty($opts['create'])) {
                $create = $opts['create'];
                if($create || $admin) {
                    $disabled = false;
                }
            }

            if(!empty($opts['reinit'])) {
                $reinit = $opts['reinit'];
            }

        }

        if($admin && $reinit) {
            $builder
                ->add('password', RepeatedType::class, array(
                    'type' => PasswordType::class,
                    'invalid_message' => 'Les mots de passe doivent correspondre',
                    'options' => array(
                        'required' => true),
                        'first_options'  => array('label' => 'Nouveau mot de passe'),
                    'second_options' => array('label' => 'Merci de le confirmer')
                ));
        } else {
            $builder
                ->add('username', TextType::class, array(
                    'label' => "Votre identifiant",
                    'disabled' => $disabled
                    ))
                ->add('firstName', TextType::class, array(
                    'label' => "Votre prénom"
                    ))
                ->add('lastName', TextType::class, array(
                    'label' => "Votre nom"
                    ))
                ->add('email', EmailType::class, array(
                    'label' => 'Votre adresse mail'
                    ));

            if($admin && $create) {
                $builder
                    ->add('password', RepeatedType::class, array(
                        'type' => PasswordType::class,
                        'invalid_message' => 'Les mots de passe doivent correspondre',
                        'options' => array(
                            'required' => true),
                            'first_options'  => array('label' => 'Nouveau mot de passe'),
                        'second_options' => array('label' => 'Merci de le confirmer')
                    ))
                    ->add('active', null, array(
                        'label' => "Actif ?",
                    ))
                    ->add('roles', EntityType::class, array(
                        'class' => 'CoudAppBundle:Role',
                        'choice_label' => 'name',
                        'multiple' => true,
                        'expanded' => true,
                    ));
            }

        }

            $builder
            ->add('submit', SubmitType::class, array(
                    'label' => 'Mise à jour',
                    'attr' => array(
                        'class' => 'btn btn-primary'
                        )
                ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Coud\AppBundle\Entity\User',
            'options' => null
        ));
    }
}
