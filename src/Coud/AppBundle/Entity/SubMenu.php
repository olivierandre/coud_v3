<?php

namespace Coud\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * SubMenu
 *
 * @ORM\Table("submenu")
 * @ORM\Entity(repositoryClass="Coud\AppBundle\Entity\SubMenuRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("title", message="Ce menu existe déjà")
 */
class SubMenu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255)
     */
    private $link;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreated", type="datetime")
     */
    private $dateCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateModified", type="datetime")
     */
    private $dateModified;

    /**
     *
     * @ORM\ManyToMany(targetEntity="MainMenu", mappedBy="subMenu", cascade={"persist"})
     * 
     */
   private $mainMenu;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set title
     *
     * @param string $title
     * @return SubMenu
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return SubMenu
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateModified
     * @return SubMenu
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime 
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * @ORM\PrePersist
     */
    public function beforeInsert() {
        $this->setDateCreated(new \DateTime());
        $this->setDateModified(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function beforeEdit() {
        $this->setDateModified(new \DateTime());
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mainMenu = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add mainMenu
     *
     * @param \Coud\AppBundle\Entity\MainMenu $mainMenu
     * @return SubMenu
     */
    public function setMainMenu(\Coud\AppBundle\Entity\MainMenu $mainMenu)
    {
        $this->mainMenu[] = $mainMenu;

        return $this;
    }

    /**
     * Remove mainMenu
     *
     * @param \Coud\AppBundle\Entity\MainMenu $mainMenu
     */
    public function removeMainMenu(\Coud\AppBundle\Entity\MainMenu $mainMenu)
    {
        $this->mainMenu->removeElement($mainMenu);
    }

    /**
     * Get mainMenu
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMainMenu()
    {
        return $this->mainMenu;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return SubMenu
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Add mainMenu
     *
     * @param \Coud\AppBundle\Entity\MainMenu $mainMenu
     * @return SubMenu
     */
    public function addMainMenu(\Coud\AppBundle\Entity\MainMenu $mainMenu)
    {
        $this->mainMenu[] = $mainMenu;

        return $this;
    }
}
