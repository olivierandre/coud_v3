<?php

namespace Coud\AppBundle\Entity;

use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="role")
 * @ORM\Entity(repositoryClass="Coud\AppBundle\Entity\RoleRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("role", message="Ce rôle existe déjà")
 * @UniqueEntity("name", message="Ce nom de rôle est déjà existant")
 * @UniqueEntity("priority", message="Cette priorité est déjà utilisée")
 */
class Role implements RoleInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=30)
     * @Assert\NotBlank(message="Veuillez fournir un nom au rôle")
     * @Assert\Length(
     *          min="4",
     *          max="255",
     *          minMessage="Le rôle doit faire au moins {{ limit }} caractères",
     *          maxMessage="le rôle ne peut pas être plus long que {{ limit }} caractères"
     * )
     */
    private $name;

    /**
     * @var integer
     * @Assert\NotBlank(message="Veuillez saisir une priorité")
     * @Assert\GreaterThan(
     *          value=-1,
     *          message="La priorité doit être supérieur à 0"
     * )
     *
     * @ORM\Column(name="priority", type="integer")
     */
    private $priority;

    /**
     * @ORM\Column(name="role", type="string", length=20, unique=true)
     * @Assert\NotBlank(message="Veuillez fournir un rôle")
     * @Assert\Length(
     *          max="255",
     *          maxMessage="le rôle ne peut pas être plus long que {{ limit }} caractères"
     * )
     * @Assert\Regex(
     *     pattern="/ROLE_[A-Z]{3,}/",
     *     message="Le nom du rôle doit toujours commencer par 'ROLE_', ne comporter que des majuscules et comporter au moins 8 caractères"
     * )
     */
    private $role;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="roles")
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @see RoleInterface
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return Group
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Add users
     *
     * @param \Coud\AppBundle\Entity\User $users
     * @return Group
     */
    public function addUser(\Coud\AppBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Coud\AppBundle\Entity\User $users
     */
    public function removeUser(\Coud\AppBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     * @return Role
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer 
     */
    public function getPriority()
    {
        return $this->priority;
    }
}
