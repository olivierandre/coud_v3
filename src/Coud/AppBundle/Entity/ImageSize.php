<?php

namespace Coud\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Cocur\Slugify\Slugify;

/**
 * ImageSize
 *
 * @ORM\Table(name="imagesize")
 * @ORM\Entity(repositoryClass="Coud\AppBundle\Entity\ImageSizeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ImageSize
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Titre obligatoire")
     * @Assert\Length(
     *          min="4",
     *          max="255",
     *          minMessage="Le titre doit faire au moins {{ limit }} caractères",
     *          maxMessage="Le titre ne peut pas être plus long que {{ limit }} caractères"
     * )
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var integer
     * @Assert\NotBlank(message="Longueur obligatoire")
     * @Assert\Range(
     *      min = 100,
     *      max = 1024,
     *      minMessage = "La longueur doit être supérieur à {{ limit }} px",
     *      maxMessage = "La longueur doit être inférieur à {{ limit}} px"
     * )
     *
     * @ORM\Column(name="width", type="smallint")
     */
    private $width;

    /**
     * @var integer
     * @Assert\NotBlank(message="Largeur obligatoire")
     * @Assert\Range(
     *      min = 100,
     *      max = 1024,
     *      minMessage = "La largeur doit être supérieur à {{ limit }} px",
     *      maxMessage = "La largeur doit être inférieur à {{ limit }} px"
     * )
     *
     * @ORM\Column(name="height", type="smallint")
     */
    private $height;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     * 
     *
     * @ORM\Column(name="slug")
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreated", type="datetime")
     */
    private $dateCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateModified", type="datetime")
     */
    private $dateModified;

    /**
     * @var \Image
     *
     * @ORM\OneToMany(targetEntity="Image", mappedBy="imageSize")
     */
    private $image;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return ImageSize
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set width
     *
     * @param integer $width
     * @return ImageSize
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return integer 
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param integer $height
     * @return ImageSize
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return integer 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ImageSize
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return ImageSize
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateModified
     * @return ImageSize
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime 
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * @ORM\PrePersist
     */
    public function beforeInsert() {
        $this->setDateCreated(new \DateTime());
        $this->setDateModified(new \DateTime());
        $slug = new Slugify();
        $this->slug = $slug->slugify($this->title);
    }

    /**
     * @ORM\PreUpdate
     */
    public function beforeEdit() {
        $this->setDateModified(new \DateTime());
    }

    public function __toString()
    {
        return $this->getTitle().' ('.$this->getWidth().' x '.$this->getHeight().')';
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->image = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return ImageSize
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add image
     *
     * @param \Coud\AppBundle\Entity\Image $image
     * @return ImageSize
     */
    public function addImage(\Coud\AppBundle\Entity\Image $image)
    {
        $this->image[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \Coud\AppBundle\Entity\Image $image
     */
    public function removeImage(\Coud\AppBundle\Entity\Image $image)
    {
        $this->image->removeElement($image);
    }

    /**
     * Get image
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImage()
    {
        return $this->image;
    }
}
