<?php

namespace Coud\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use abeautifulsite\SimpleImage;
use Cocur\Slugify\Slugify;

/**
 * Image
 *
 * @ORM\Table(name="image")
 * @ORM\Entity(repositoryClass="Coud\AppBundle\Entity\ImageRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("title", message="Ce nom d'image existe déjà")
 */
class Image
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Titre obligatoire (Nécessaire pour le référencement)")
     * @Assert\Length(
     *          min="4",
     *          max="255",
     *          minMessage="Le titre de l'image doit faire au moins {{ limit }} caractères",
     *          maxMessage="Le titre de l'image ne peut pas être plus long que {{ limit }} caractères"
     * )
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     * @Assert\NotBlank(message="Texte alternatif obligatoire (Nécessaire pour le référencement)")
     * @Assert\Length(
     *          min="4",
     *          max="255",
     *          minMessage="Le texte alternatif doit faire au moins {{ limit }} caractères",
     *          maxMessage="Le texte alternatif ne peut pas être plus long que {{ limit }} caractères"
     * )
     *
     * @ORM\Column(name="alt", type="string", length=255)
     */
    private $alt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @Assert\Image(
     *            maxSize="5000000",
     *            mimeTypesMessage="Mauvais format. Seules les images sont acceptées",
     *            maxSizeMessage="Le taille du fichier ne peut excéder les 5Mo"
     * )
     * @Assert\NotBlank(message="Fichier obligatoire !!!")
     */
    public $file;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreated", type="datetime")
     */
    private $dateCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateModified", type="datetime")
     */
    private $dateModified;

    /**
     * @var \ImageSize
     * @ORM\ManyToOne(targetEntity="ImageSize", inversedBy="image")
     * @ORM\JoinColumn(name="size", nullable=false)
     *
     */
    private $imageSize;

    /**
     * @var \Article
     * @ORM\OneToMany(targetEntity="Article", mappedBy="image")
     */
    private $imageArticle;

    /**
     * @var \Article
     * @ORM\ManyToMany(targetEntity="Article", mappedBy="images")
     */
    private $article;

    private $realPath;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Image
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set alt
     *
     * @param string $alt
     * @return Image
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get alt
     *
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return Image
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateModified
     * @return Image
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }


    /**
     * @ORM\PrePersist
     */
    public function beforeInsert() {
        $this->setDateCreated(new \DateTime());
        $this->setDateModified(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function beforeEdit() {
        $this->setDateModified(new \DateTime());
    }


    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path ? null : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // le chemin absolu du répertoire où les documents uploadés doivent être sauvegardés
        return $this->getRealPath().'/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // on se débarrasse de « __DIR__ » afin de ne pas avoir de problème lorsqu'on affiche
        // le document/image dans la vue.
        return 'images';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            // faites ce que vous voulez pour générer un nom unique
            $slug = new Slugify();
            $slug = $slug->slugify($this->title);
            $this->path = $slug.'.'.$this->file->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $size = $this->imageSize;
        $width = $size->getWidth();
        $height = $size->getHeight();
        $img = new SimpleImage($this->file);

        $defaultWidth = $img->get_width();
        $defaultHeight = $img->get_height();

        if($defaultHeight > $defaultWidth) {
            $defaultWidth = $defaultHeight;
            $defaultHeight = $img->get_width();
        }

        $img->best_fit($width, $height);
        //  Traitement Retina
        $retina = new SimpleImage($this->file);
        $retina->best_fit($width * 2, $height * 2);

        //  Si c'est un theme
        if($size->getSlug() === "theme") {
            $dir = "theme";
            //  création de l'image pour "A la une"
            $important = new SimpleImage($this->file);
            $width = $defaultWidth / 7;
            $height = $defaultHeight / 7;

            // Version Retina
            $importantRetina = new SimpleImage($this->file);

            //  Sauvegarde complète
            $important->best_fit($width, $height)->save($this->getUploadRootDir().'/important/'.$this->path);
            $importantRetina->best_fit($width * 2, $height * 2)->save($this->getUploadRootDir().'/important/retina/'.$this->path);

        } else {
            $dir = "article";
        }

        $retina->save($this->getUploadRootDir().'/'.$dir.'/retina/'.$this->path);
        $img->save($this->getUploadRootDir().'/'.$dir.'/'.$this->path);


        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $link = $this->getImageSize()->getSlug();
        if($link == "square") {
            $link = "article";
        }

        if ($this->file = $this->getAbsolutePath()) {
            unlink("images/".$link."/".$this->getPath());
        }
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Image
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set imageSize
     *
     * @param \Coud\AppBundle\Entity\ImageSize $imageSize
     * @return Image
     */
    public function setImageSize(\Coud\AppBundle\Entity\ImageSize $imageSize)
    {
        $this->imageSize = $imageSize;

        return $this;
    }

    /**
     * Get imageSize
     *
     * @return \Coud\AppBundle\Entity\ImageSize
     */
    public function getImageSize()
    {
        return $this->imageSize;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->article = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add article
     *
     * @param \Coud\AppBundle\Entity\Article $article
     * @return Image
     */
    public function addArticle(\Coud\AppBundle\Entity\Article $article)
    {
        $this->article[] = $article;

        return $this;
    }

    /**
     * Remove article
     *
     * @param \Coud\AppBundle\Entity\Article $article
     */
    public function removeArticle(\Coud\AppBundle\Entity\Article $article)
    {
        $this->article->removeElement($article);
    }

    /**
     * Get article
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Add imageArticle
     *
     * @param \Coud\AppBundle\Entity\Article $imageArticle
     * @return Image
     */
    public function addImageArticle(\Coud\AppBundle\Entity\Article $imageArticle)
    {
        $this->imageArticle[] = $imageArticle;

        return $this;
    }

    /**
     * Remove imageArticle
     *
     * @param \Coud\AppBundle\Entity\Article $imageArticle
     */
    public function removeImageArticle(\Coud\AppBundle\Entity\Article $imageArticle)
    {
        $this->imageArticle->removeElement($imageArticle);
    }

    /**
     * Get imageArticle
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImageArticle()
    {
        return $this->imageArticle;
    }

    public function getRealPath() {
        return $this->realPath;
    }

    public function setRealPath($realPath) {
        $this->realPath = $realPath;
    }
}
