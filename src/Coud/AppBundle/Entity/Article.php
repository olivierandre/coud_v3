<?php

namespace Coud\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Cocur\Slugify\Slugify;

/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="Coud\AppBundle\Entity\ArticleRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("title", message="Ce nom d'article existe déjà")
 */
class Article
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Titre obligatoire")
     * @Assert\Length(
     *          min="4",
     *          max="255",
     *          minMessage="Le titre doit faire au moins {{ limit }} caractères",
     *          maxMessage="le titre ne peut pas être plus long que {{ limit }} caractères"
     * )
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     * @Assert\NotBlank(message="Contenu obligatoire")
     * @Assert\Length(
     *          min="10",
     *          minMessage="Le contenu doit faire au moins {{ limit }} caractères",
     * )
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var boolean
     *
     * @ORM\Column(name="published", type="boolean")
     */
    private $published;

    /**
     * @var integer
     *
     * @ORM\Column(name="view", type="integer")
     */
    private $view;

    /**
     * @var boolean
     *
     * @ORM\Column(name="important", type="boolean")
     */
    private $important;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreated", type="datetime")
     */
    private $dateCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateModified", type="datetime")
     */
    private $dateModified;

    /**
     * @var \Image
     * @ORM\ManyToOne(targetEntity="Image", inversedBy="imageArticle")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Choix obligatoire !!!")
     */
    private $image;

    /**
     * @var \Image
     * @ORM\ManyToMany(targetEntity="Image", inversedBy="article")
     * 
     */
    private $images;

    /**
     * @var \User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     * 
     */
    private $author;

    /**
     * @var \MainMenu
     * @ORM\ManyToOne(targetEntity="MainMenu")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Choix obligatoire !!!")
     * 
     */
    private $mainMenu;

    /**
     * @var \SubMenu
     * @ORM\ManyToOne(targetEntity="SubMenu")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Choix obligatoire !!!")
     * 
     */
    private $subMenu;

    /**
     * @var \Comment
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="article")
     */
    private $comments;

    /**
     * @var \TypeArticle
     * @ORM\ManyToOne(targetEntity="TypeArticle", inversedBy="articles")
     * @ORM\JoinColumn(nullable=true)
     * 
     */
    private $typeArticle;

    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Article
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Article
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set view
     *
     * @param integer $view
     * @return Article
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get view
     *
     * @return integer 
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return Article
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateModified
     * @return Article
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime 
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * @ORM\PrePersist
     */
    public function beforeInsert() {
        $this->setDateCreated(new \DateTime());
        $this->setDateModified(new \DateTime());
        $slug = new Slugify();
        $this->slug = $slug->slugify($this->title);
        $this->view = 0;
        $this->published = false;
        $this->important = false;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set image
     *
     * @param \Coud\AppBundle\Entity\Image $image
     * @return Article
     */
    public function setImage(\Coud\AppBundle\Entity\Image $image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Coud\AppBundle\Entity\Image 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add images
     *
     * @param \Coud\AppBundle\Entity\Image $images
     * @return Article
     */
    public function addImage(\Coud\AppBundle\Entity\Image $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \Coud\AppBundle\Entity\Image $images
     */
    public function removeImage(\Coud\AppBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set published
     *
     * @param boolean $published
     * @return Article
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean 
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set author
     *
     * @param \Coud\AppBundle\Entity\User $author
     * @return Article
     */
    public function setAuthor(\Coud\AppBundle\Entity\User $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Coud\AppBundle\Entity\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set mainMenu
     *
     * @param \Coud\AppBundle\Entity\MainMenu $mainMenu
     * @return Article
     */
    public function setMainMenu(\Coud\AppBundle\Entity\MainMenu $mainMenu)
    {
        $this->mainMenu = $mainMenu;

        return $this;
    }

    /**
     * Get mainMenu
     *
     * @return \Coud\AppBundle\Entity\MainMenu 
     */
    public function getMainMenu()
    {
        return $this->mainMenu;
    }

    /**
     * Set subMenu
     *
     * @param \Coud\AppBundle\Entity\SubMenu $subMenu
     * @return Article
     */
    public function setSubMenu(\Coud\AppBundle\Entity\SubMenu $subMenu)
    {
        $this->subMenu = $subMenu;

        return $this;
    }

    /**
     * Get subMenu
     *
     * @return \Coud\AppBundle\Entity\SubMenu 
     */
    public function getSubMenu()
    {
        return $this->subMenu;
    }

    /**
     * Set important
     *
     * @param boolean $important
     * @return Article
     */
    public function setImportant($important)
    {
        $this->important = $important;

        return $this;
    }

    /**
     * Get important
     *
     * @return boolean 
     */
    public function isImportant()
    {
        return $this->important;
    }

    /**
     * Get important
     *
     * @return boolean 
     */
    public function getImportant()
    {
        return $this->important;
    }

    /**
     * Add comments
     *
     * @param \Coud\AppBundle\Entity\Comment $comments
     * @return Article
     */
    public function addComment(\Coud\AppBundle\Entity\Comment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \Coud\AppBundle\Entity\Comment $comments
     */
    public function removeComment(\Coud\AppBundle\Entity\Comment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set typeArticle
     *
     * @param \Coud\AppBundle\Entity\TypeArticle $typeArticle
     *
     * @return Article
     */
    public function setTypeArticle(\Coud\AppBundle\Entity\TypeArticle $typeArticle)
    {
        $this->typeArticle = $typeArticle;

        return $this;
    }

    /**
     * Get typeArticle
     *
     * @return \Coud\AppBundle\Entity\TypeArticle
     */
    public function getTypeArticle()
    {
        return $this->typeArticle;
    }
}
