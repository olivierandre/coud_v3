<?php

namespace Coud\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Menu
 *
 * @ORM\Table("mainmenu")
 * @ORM\Entity(repositoryClass="Coud\AppBundle\Entity\MainMenuRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("title", message="Ce menu existe déjà")
 */
class MainMenu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Nom du menu obligatoire")
     * @Assert\Length(
     *          min="4",
     *          max="255",
     *          minMessage="Le nom du menu principal doit faire au moins {{ limit }} caractères",
     *          maxMessage="le nom du menu principal ne peut pas être plus long que {{ limit }} caractères"
     * )
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255)
     */
    private $link;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreated", type="datetime")
     */
    private $dateCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateModified", type="datetime")
     */
    private $dateModified;

    /**
     *
     * @ORM\ManyToMany(targetEntity="SubMenu", inversedBy="mainMenu")
     * @ORM\JoinTable(name="mainmenu_submenu",
     *      joinColumns={@ORM\JoinColumn(name="mainmenu_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="submenu_id", referencedColumnName="id")}
     * )
     */
    private $subMenu;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Menu
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Menu
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return Menu
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateModified
     * @return Menu
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime 
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * @ORM\PrePersist
     */
    public function beforeInsert() {
        $this->setDateCreated(new \DateTime());
        $this->setDateModified(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function beforeEdit() {
        $this->setDateModified(new \DateTime());
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subMenu = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add subMenu
     *
     * @param \Coud\AppBundle\Entity\SubMenu $subMenu
     * @return MainMenu
     */
    public function addSubMenu(\Coud\AppBundle\Entity\SubMenu $subMenu)
    {
        $this->subMenu[] = $subMenu;

        return $this;
    }

    /**
     * Remove subMenu
     *
     * @param \Coud\AppBundle\Entity\SubMenu $subMenu
     */
    public function removeSubMenu(\Coud\AppBundle\Entity\SubMenu $subMenu)
    {
        $this->subMenu->removeElement($subMenu);
    }

    /**
     * Get subMenu
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSubMenu()
    {
        return $this->subMenu;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return MainMenu
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }
}
