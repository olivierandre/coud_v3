<?php

namespace Coud\AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use abeautifulsite\SimpleImage;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * Aside
 */
class Aside
{
    /**
     * @var integer
     *
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Titre obligatoire")
     * @Assert\Length(
     *          min="4",
     *          max="255",
     *          minMessage="Le titre doit faire au moins {{ limit }} caractères",
     *          maxMessage="le titre ne peut pas être plus long que {{ limit }} caractères"
     * )
     *
     */
    private $title;

    /**
     * @var string
     * @Assert\NotBlank(message="Contenu obligatoire")
     * @Assert\Length(
     *          min="10",
     *          minMessage="Le contenu doit faire au moins {{ limit }} caractères",
     * )
     */
    private $content;

    /**
     * @var string
     *
     */
    private $path;

    /**
     * @Assert\Image(
     *            maxSize="5000000",
     *            mimeTypesMessage="Mauvais format. Seules les images sont acceptées",
     *            maxSizeMessage="Le taille du fichier ne peut excéder les 5Mo"
     * )
     */
    public $file;

    private $realPath;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Aside
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Aside
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Aside
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path ? null : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // le chemin absolu du répertoire où les documents uploadés doivent être sauvegardés
        return $this->getRealPath().'/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // on se débarrasse de « __DIR__ » afin de ne pas avoir de problème lorsqu'on affiche
        // le document/image dans la vue.
        return 'images';
    }

    /**
     * 
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        // s'il y a une erreur lors du déplacement du fichier, une exception
        // va automatiquement être lancée par la méthode move(). Cela va empêcher
        // proprement l'entité d'être persistée dans la base de données si
        // erreur il y a
        $dir = "aside";
        $img = new SimpleImage($this->file);
        $width = 300;
        $height = 200;
        $img->best_fit($width, $height);

        //  Traitement Retina
        $retina = new SimpleImage($this->file);
        $retina->best_fit($width * 2, $height * 2);

        if(!file_exists($this->getUploadRootDir())) {
            if(!mkdir($this->getUploadRootDir())) {
                throw new FileException('Erreur lors de la création du répertoire');
            }
            
        }
        
        $retina->save($this->getUploadRootDir().'/'.$dir.'/retina/'.$this->path);
        $img->save($this->getUploadRootDir().'/'.$dir.'/'.$this->path);
        
        
        unset($this->file);
    }

    /**
     * 
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    public function getRealPath() {
        return $this->realPath;
    }

    public function setRealPath($realPath) {
        $this->realPath = $realPath;
    }

}
