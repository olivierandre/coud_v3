<?php

namespace Coud\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Config
 *
 * @ORM\Table(name="config")
 * @ORM\Entity(repositoryClass="Coud\AppBundle\Repository\ConfigRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Config
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="limitImportant", type="smallint")
     * @Assert\NotBlank(message="Valeur obligatoire")
     * @Assert\Length(
     *          min="1",
     *          max="10",
     *          minMessage="Il est nécessaire que la limite soit de {{ limit }} au minimum",
     *          maxMessage="Il est nécessaire que la limite soit de {{ limit }} au maximum"
     * )
     */
    private $limitImportant;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set limitImportant
     *
     * @param integer $limitImportant
     *
     * @return Config
     */
    public function setLimitImportant($limitImportant)
    {
        $this->limitImportant = $limitImportant;

        return $this;
    }

    /**
     * Get limitImportant
     *
     * @return int
     */
    public function getLimitImportant()
    {
        return $this->limitImportant;
    }
}
