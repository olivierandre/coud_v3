<?php
namespace Coud\AppBundle\Twig;

class EscapeFilter extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('stripTagsAndHtmlEntityDecodeFilter', array($this, 'stripTagsAndHtmlEntityDecodeFilter')),
        );
    }

    public function stripTagsAndHtmlEntityDecodeFilter($content)
    {
        $text = str_replace('&nbsp;', ' ', $content);
        return trim(strip_tags(html_entity_decode($text)));
    }

    public function getName()
    {
        return 'escape_filter';
    }
}
