<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/admin")
 *
 */
class AdminController extends Controller
{
    /**
     * @Route("/")
     * @Method({"GET"})
     */
    public function homeAction()
    {
        return $this->render('admin/index.html.twig', array());
    }

}
