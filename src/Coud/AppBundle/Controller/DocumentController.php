<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Coud\AppBundle\Entity\Comment;
use Coud\AppBundle\Form\Type\CommentType;
use Coud\AppBundle\Service\DocumentService;

use Coud\AppBundle\Service\DetectMobileService;
/**
 * @Method({"GET"})
 */
class DocumentController extends Controller
{
    /**
     * @Route("/{mainMenu}/{subMenu}/{slug}", requirements={"mainMenu": "^(?!.*(admin|document[a-zA-Z]*)).*"})
     *
     */
    public function showDocumentAction($mainMenu, $subMenu, $slug, Request $request, DocumentService $documentService, DetectMobileService $detectMobileService)
    {
        try {
            $article = $this->getDoctrine()->getRepository('CoudAppBundle:Article')->findDocumentActifBySlug($mainMenu, $subMenu, $slug);
        } catch (\Doctrine\ORM\NoResultException $e) {
            throw $this->createNotFoundException('Article inconnu');
        }

        try {
            //$documentService = $this->get('document_service');
            $documentService->updateViewDocument($article);
        } catch(\Exception $e) {
            throw new \Exception('Une erreur est intervenue pendant un accés à la base de données'.$e);
        }

        $commentForm = $this->createForm(CommentType::class, new Comment());

        $userAgent = $request->server->get('HTTP_USER_AGENT');
        $isMobileOrTablet = $detectMobileService->isMobileOrTablet($userAgent);
        $isMobile = $detectMobileService->isMobile($userAgent);

        $routeName = $request->getUri();
        return $this->render('coud/article/document.html.twig', array(
            'article' => $article,
            'mainMenuSlug' => $mainMenu,
            'subMenuSlug' => $subMenu,
            'commentForm' => $commentForm->createView(),
            'slug' => $slug,
            'isMobileOrTablet' => $isMobileOrTablet,
            'isMobile' => $isMobile,
            'routeName' => $routeName
        ));
    }

    /**
     * @Route("/document/verify_comment/{mainMenu}/{subMenu}/{slug}")
     * @Method({"POST"})
     */
    public function setCommentAction($mainMenu, $subMenu, $slug, Request $request) {

        if($request->isXmlHttpRequest()) {

            $comment = new Comment();
            $commentForm = $this->createForm(CommentType::class, $comment);
            $commentForm->handleRequest($request);
            $response = new JsonResponse();

            if($commentForm->isSubmitted() && $commentForm->isValid()) {
                try {
                    $article = $this->getDoctrine()->getRepository('CoudAppBundle:Article')->findDocumentActifBySlug($mainMenu, $subMenu, $slug);
                } catch (\Doctrine\ORM\NoResultException $e) {
                    return $response->setData(array(
                        'result' => 2,
                        'response' => 'Article inconnu'
                    ));
                }
                try {
                    $comment->setArticle($article);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($comment);
                    $em->flush();

                    //  Envoi du mail
                    $serviceMail = $this->get('mail_service');
                    $serviceMail->sendMailComment($article->getTitle());

                    return $response->setData(array(
                        'result' => 1,
                        'response' => 'Votre commentaire a été pris en compte. Il doit être validé par la modération du site.'
                    ));


                } catch(\Doctrine\ORM\ORMException $e) {
                    return $response->setData(array(
                        'result' => 2,
                        'response' => 'Une erreur est intervenue : '.$e
                    ));

                }
            } else {
                $service = $this->get('form_service');
                $errors = $service->getErrorsAsArray($commentForm);
                return $response->setData(array(
                    'result' => 0,
                    'errors' => $errors
                ));
            }

        } else {
            //  Renvoi sur la home
            return $this->redirect($this->generateUrl('coud_app_home_index'));
        }
    }

}
