<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/admin/comment")
 */
class AdminCommentController extends Controller
{
    /**
     * @Route("/")
     * @Method({"GET"})
     */
    public function homeAction()
    {
        return $this->render('admin/comment/index.html.twig', array());
    }

    /**
     * @Route("/en-attente-de-moderation")
     * @Method({"GET"})
     */
    public function waitCommentAction()
    {
        $comments = $this->getDoctrine()->getRepository('CoudAppBundle:Comment')->findByModerationWithArticle(false);
        $params = array(
            'comments' => $comments,
        );

        return $this->render('admin/comment/wait.html.twig', $params);
    }

    /**
     * @Route("/validation/{id}")
     * @Method({"GET","POST"})
     */
    public function validCommentAction($id)
    {
        $comment = $this->getDoctrine()->getRepository('CoudAppBundle:Comment')->findOneById($id);

        if (!$comment) {
            throw $this->createNotFoundException('Aucun commentaire trouvé pour cet id : '.$id);
        }
        $comment->setModeration(true);

        try {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
        } catch (\Doctrine\ORM\ORMException $e) {
            $this->get('session')->getFlashBag()->add(
                    'errorMessage',
                    'Une erreur est intervenue : '.$e
                );
        }

        $this->get('session')->getFlashBag()->add(
            'validMessage',
            "Le commentaire '".$comment->getId()."' a bien été activé"
        );

        return $this->redirect($this->generateUrl('coud_app_admincomment_waitcomment'));
    }

    /**
     * @Route("/suppression/{id}")
     * @Method({"GET","POST"})
     */
    public function removeCommentAction($id)
    {
        $comment = $this->getDoctrine()->getRepository('CoudAppBundle:Comment')->findOneById($id);

        if (!$comment) {
            throw $this->createNotFoundException('Aucun commentaire trouvé pour cet id : '.$id);
        }

        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comment);
            $em->flush();
        } catch (\Doctrine\ORM\ORMException $e) {
            $this->get('session')->getFlashBag()->add(
                    'errorMessage',
                    'Une erreur est intervenue : '.$e
                );
        }

        $this->get('session')->getFlashBag()->add(
            'validMessage',
            "Le commentaire '".$comment->getId()."' a bien été supprimé"
        );

        return $this->redirect($this->generateUrl('coud_app_admincomment_waitcomment'));
    }

    /**
     * @Route("/liste")
     * @Method({"GET"})
     */
    public function listCommentAction()
    {
        $comments = $this->getDoctrine()->getRepository('CoudAppBundle:Comment')->findByModeration(true);

        return $this->render('admin/comment/list.html.twig', array('comments' => $comments));
    }

    /**
     * @Route("/detail/{id}")
     * @Method({"GET"})
     */
    public function detailCommentAction($id)
    {
        $comment = $this->getDoctrine()->getRepository('CoudAppBundle:Comment')->findOneById($id);

        return $this->render('admin/comment/detail.html.twig', array('comment' => $comment));
    }
}
