<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Coud\AppBundle\Entity\Config;
use Coud\AppBundle\Form\Type\ConfigType;

/**
 * @Route("/admin/parameters")
 *
 */
class AdminParametersController extends Controller
{
    /**
     * @Route("/")
     * @Method({"GET", "POST"})
     */
    public function parametersAction(Request $request)
    {
        $config = $this->getDoctrine()->getRepository("CoudAppBundle:Config")->findOneById(1);

        if($config === null) {
            $config = new Config();
        }
    	$configForm = $this->createForm(ConfigType::class, $config);
        $configForm->handleRequest($request);

        if($configForm->isSubmitted() && $configForm->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($config);
                $em->flush();

            } catch(\Doctrine\ORM\ORMException $e) {
                $this->get('session')->getFlashBag()->add(
                    'errorMessage',
                    "Une erreur est intervenue : ".$e
                );
            }

            $this->get('session')->getFlashBag()->add(
                'validMessage',
                'Mise à jour des paramétres effectuées.'
            );
            return $this->redirect($this->generateUrl('coud_app_adminparameters_parameters'));
        }

        return $this->render('admin/parameters/parameters.html.twig', array(
            'configForm' => $configForm->createView()
        ));
    }

}
