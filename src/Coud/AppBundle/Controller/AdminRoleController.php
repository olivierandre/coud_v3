<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

use Coud\AppBundle\Entity\Role;
use Coud\AppBundle\Form\Type\RoleType;

/**
 * @Route("/admin/role")
 * @Security("has_role('ROLE_ADMIN')")
 * @Method({"GET"})
 */
class AdminRoleController extends Controller
{
    /**
     * @Route("/liste-des-roles")
     *
     */
    public function listRolesAction()
    {
        $roles = $this->getDoctrine()->getRepository('CoudAppBundle:Role')->findAll();

        return $this->render('admin/role/index_role.html.twig', array('roles' => $roles));
    }

    /**
     * @Route("/creation-role")
     * @Method({"GET", "POST"})
     */
    public function createRoleAction(Request $request)
    {
        $formParams = array(
            "modify" => false,
            );
        $role = new Role();
        $roleForm = $this->createForm(RoleType::class, $role, array('options' => $formParams));
        $roleForm->handleRequest($request);

        if($roleForm->isSubmitted() && $roleForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($role);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'validMessage',
                    "Nouveau rôle créé"
                );
            return $this->redirect($this->generateUrl('coud_app_adminrole_listroles'));
        }

        return $this->render('admin/role/create_role.html.twig', array('roleForm' => $roleForm->createView()));
    }

    /**
     * @Route("/modification-role/{id}")
     * @Method({"GET", "PUT"})
     */
    public function modifyRoleAction($id, Request $request)
    {
        $role = new Role();

        $formParams = array(
            "modify" => true,
            );

        $doctrine = $this->getDoctrine();
        $role = $doctrine->getRepository('CoudAppBundle:Role')->findOneById($id);

        if(!$role) {
            throw $this->createNotFoundException('Aucun rôle trouvé pour cet id : '.$id);
        }

        $roleForm = $this->createForm(RoleType::class, $role, array(
            'options' => $formParams,
            'method' => 'PUT'
        ));
        $roleForm->handleRequest($request);

        if($roleForm->isSubmitted() && $roleForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($role);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'validMessage',
                    "Rôle '".$role->getName()."' a été mis à jour"
                );
            return $this->redirect($this->generateUrl('coud_app_adminrole_listroles'));
        }

        return $this->render('admin/role/modify_role.html.twig', array('roleForm' => $roleForm->createView()));
    }

}
