<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Session\Session;
use Coud\AppBundle\Service\ToastrService;

/**
 * @Route("/admin")
 *
 */
class AdminCacheController extends Controller
{
    private $session;
    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * @Route("/cache")
     * @Method({"GET"})
     */
    public function cacheClearAction(ToastrService $toastrService)
    {
        $type = $this->session->get('type');
        if ($type) {
            $sessionMessage = $toastrService->setMessage('success', 'Suppression du cache "' . $type . '" réalisé', 'Gestion du cache');
            $this->session->remove('type');
        }

        return $this->render('admin/cache/index.html.twig', [
          'sessionMessage' => isset($sessionMessage) ? $sessionMessage : ''
        ]);
    }

    /**
     * @Route("/cache/clear/{type}")
     * @Method({"GET"})
     */
    public function cacheClearTypeAction($type = null)
    {
        $this->session = new Session();

        if ($type === 'apc') {
            apcu_clear_cache();
            $this->session->set('type', $type);
        }

        return $this->redirect($this->generateUrl('coud_app_admincache_cacheclear'));
    }
}
