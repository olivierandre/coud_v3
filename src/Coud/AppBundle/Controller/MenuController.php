<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Coud\AppBundle\Entity\Comment;
use Coud\AppBundle\Form\Type\CommentType;
use Coud\AppBundle\Service\ToastrService;


/**
 * @Method({"GET"})
 */
class MenuController extends Controller
{
    public function getAllMenusAction()
    {
        $menus = $this->getDoctrine()->getRepository('CoudAppBundle:MainMenu')->findAllMenuActive();

        return $this->render('CoudAppBundle:Menu:navbar.html.twig', array('menus' => $menus));
    }

    /**
     * @Route("/contact")
     * @Method({"GET", "POST"})
     */
    public function contactAction(Request $request, ToastrService $toastrService)
    {
        $comment = new Comment();
        $sessionMessage = [];
        $commentForm = $this->createForm(CommentType::class, $comment);
        $commentForm->handleRequest($request);

        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            //  Envoi du mail
            $serviceMail = $this->get('mail_service');
            $send = $serviceMail->sendMailContact($comment);

            if ($send) {
                $sessionMessage = $toastrService->setMessage('success', 'Votre message a été transmis. Une réponse vous sera apportée rapidement :-)', 'Contact');
                $commentForm = $this->createForm(CommentType::class, new Comment());
            } else {
                $sessionMessage = $toastrService->setMessage('error', 'Une erreur est intervenue. Merci d\'effectuer une nouvelle demande', 'Problème');
            }
        }

        return $this->render('coud/contact.html.twig', array(
            'commentForm' => $commentForm->createView(),
            'sessionMessage' => $sessionMessage,
        ));
    }

    /**
     * @Route("/{mainMenu}/{subMenu}", requirements={"mainMenu": "^(?!.*(admin[a-zA-Z]*)).*"})
     */
    public function showThemeAction($mainMenu, $subMenu)
    {
        $articles = $this->getDoctrine()->getRepository('CoudAppBundle:Article')->findDocumentsByMainMenuAndSubMenu($mainMenu, $subMenu);

        return $this->render('coud/theme.html.twig', array(
            'articles' => $articles,
            'mainMenuSlug' => $mainMenu,
            'subMenuSlug' => $subMenu,
        ));
    }

    /**
     * @Route("/{mainMenu}", requirements={"mainMenu": "^(?!.*(admin[a-zA-Z]*)).*"})
     */
    public function showAllThemeAction($mainMenu)
    {
        $articles = $this->getDoctrine()->getRepository('CoudAppBundle:Article')->findDocumentsByMainMenu($mainMenu);

        return $this->render('coud/theme.html.twig', array(
            'articles' => $articles,
            'mainMenuSlug' => $mainMenu,
            'all' => true,
        ));
    }
}
