<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

use Coud\AppBundle\Service\DetectMobileService;

class HomeController extends Controller
{
    /**
     * @Route("/")
     * @Method({"GET"})
     */
    public function indexAction(Request $request, DetectMobileService $detectMobileService)
    {
        apcu_clear_cache();
        $doctrine = $this->getDoctrine();
        $lastDocuments = $doctrine->getRepository('CoudAppBundle:Article')->findLastDocuments();
        $isMobile = $detectMobileService->isMobile($request->server->get('HTTP_USER_AGENT'));

        $response = $this->render('coud/index.html.twig', [
            'lastDocuments' => $lastDocuments,
            'isMobile' => $isMobile,
            'isHomepage' => true
        ]);

        // cache for 3600 seconds
        $response->setSharedMaxAge(3600);

        // (optional) set a custom Cache-Control directive
        $response->headers->addCacheControlDirective('must-revalidate', true);

        return $response;
    }

    /**
     * @Route("/api/getImportantsDocuments")
     * @Method({"GET"})
     */
    public function getImportantsDocumentsAction(Request $request)
    {
        $doctrine = $this->getDoctrine();
        $importants = $doctrine->getRepository('CoudAppBundle:Article')->findDocumentsByImportant();

        if (count($importants) === 0) {
            $importants = $doctrine->getRepository('CoudAppBundle:Article')->findDocumentsByDateModified();
        }

        return $this->render('coud/importants_documents.html.twig', [
          'importants' => $importants
        ]);
    }
}
