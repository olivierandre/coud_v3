<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Coud\AppBundle\Entity\Aside;
use Coud\AppBundle\Service\JsonService;

class JsonController extends Controller
{
    const TIMEOUT = 86400;

    public function legalNoticesAction(JsonService $jsonService) {
        if(!apcu_fetch('notices')) {
            apcu_delete('notices');
            $filename = 'legalNotices.json';
            //  En cache pendant une journée
            apcu_store('notices', $jsonService->getJson($filename), self::TIMEOUT);
        }

        return $this->render('CoudAppBundle:Document:legal.html.twig', array('notices' => apcu_fetch('notices')));
    }

    /**
     * @Route("/qui-suis-je/{reset}")
     * @Method({"GET"})
     */
    public function whoIAmAction($reset = false, JsonService $jsonService) {
        if(!apcu_fetch('aside') || $reset) {
            apcu_delete('aside');
            $filename = 'aside.json';
            $json = $jsonService->getJson($filename);

            $aside = new Aside();
            $aside->setTitle($json['title']);
            $aside->setContent($json['content']);
            $aside->setPath($json['path']);
            apcu_store('aside', $aside, self::TIMEOUT);
        }

        return $this->render('coud/whoiam.html.twig', array('aside' => apcu_fetch('aside')));
    }

}
