<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

use Coud\AppBundle\Entity\Aside;
use Coud\AppBundle\Form\Type\AsideType;
use Coud\AppBundle\Service\JsonService;

/**
 * @Route("/admin")
 * @Method({"GET"})
 */
class AdminJsonController extends Controller
{

    public function legalNoticesAction(JsonService $jsonService) {
        $filename = 'legalNotices.json';
        $service = $this->get('json_service');
        $notices = $jsonService->getJson($filename);

        return $this->render('CoudAppBundle:Document:legal.html.twig', array('notices' => $notices));
    }

    /**
     * @Route("/qui-suis-je")
     * @Method({"GET", "POST"})
     */
    public function whoIAmAction(Request $request, JsonService $jsonService)
    {
        $filename = 'aside.json';
        $sessionMessage = [];
        $json = $jsonService->getJson($filename);

        $aside = new Aside();
        $aside->setTitle($json['title'])
            ->setContent($json['content'])
            ->setPath($json['path']);

    	$asideForm = $this->createForm(AsideType::class, $aside, array(
            "label" => "Mettre à jour"
        ));
    	$asideForm->handleRequest($request);

    	if($asideForm->isSubmitted() && $asideForm->isValid()) {
    	   try {
                if($aside->file !== null) {
                    $imageService = $this->get('image_service');
                    $aside->setRealPath($this->get('kernel')->getRootDir(). '/../web');
                    $aside = $imageService->saveImageForAside($aside);
                }

                $json = array(
                    'title' => $aside->getTitle(),
                    'content' => $aside->getContent(),
                    'path' => $aside->getPath(),
                );

                $service->saveJson($filename, $json);

                $sessionMessage = [
                    'status' => 'success',
                    'message' => 'Article à jour'
                ];

            } catch(\Doctrine\ORM\ORMException $e) {
                $sessionMessage = [
                    'status' => 'error',
                    'message' => 'Une erreur est intervenue : '.$e
                ];
            }
        }

        return $this->render('admin/aside/aside.html.twig', array(
            "aside" => $aside,
            'asideForm' => $asideForm->createView(),
            'sessionMessage' => $sessionMessage
        ));
    }

}
