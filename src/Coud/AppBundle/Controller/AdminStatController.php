<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/admin/statistiques")
 * @Method({"GET"})
 */
class AdminStatController extends Controller
{
    /**
     * @Route("/")
     *
     */
    public function homeAction()
    {
        return $this->render('admin/stat/index.html.twig', array());
    }

    /**
     * @Route("/vues")
     * 
     */
    public function countViewAction() {
    	$articles = $this->getDoctrine()->getRepository('CoudAppBundle:Article')->findIsPublishedOrderByView();
        return $this->render('admin/stat/view.html.twig', array(
            'articles' => $articles,
            ));
    }

    /**
     * @Route("/get-data-views", name="getDataViews")
     *
     */
    public function getViewsJsonAction() {
        $articles = $this->getDoctrine()->getRepository('CoudAppBundle:Article')->findIsPublishedOrderByView();
        $service = $this->get('stat_service');
        $tab = array();

        foreach ($articles as $key => $article) {
            $tab['categories'][$key] = $article->getTitle();
            $tab['series'][0]['data'][$key] = $article->getView();
        }
        $tab['series'][0]['name'] = "Total des vues pour chaque article";

        $data = $service->createJsonForBasicColumn($tab, "Nombre de vues", "Vues");
        $response = new JsonResponse();
        $response->setData($data);
        return $response;

    }

}
