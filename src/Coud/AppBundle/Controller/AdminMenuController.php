<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

use Coud\AppBundle\Entity\MainMenu;
use Coud\AppBundle\Form\Type\MainMenuType;
use Coud\AppBundle\Form\Type\MainMenuEditType;
use Coud\AppBundle\Entity\SubMenu;
use Coud\AppBundle\Form\Type\SubMenuType;

/**
 * @Route("/admin/menu")
 * @Method({"GET"})
 */
class AdminMenuController extends Controller
{
    /**
     * @Route("/")
     *
     */
    public function homeAction()
    {
    	$params = array();

        return $this->render('admin/manageMenu/index.html.twig', $params);
    }

    /**
     * @Route("/gestion-menu-principal")
     * @Method({"GET", "POST"})
     */
    public function manageMainMenuAction(Request $request) {
    	$params = array();
    	$mainMenu = new MainMenu();

    	$mainMenuForm = $this->createForm(MainMenuType::class, $mainMenu);
        $mainMenuForm->handleRequest($request);

        if($mainMenuForm->isSubmitted() && $mainMenuForm->isValid()) {
            $slug = $this->get('slugify');
            $mainMenu->setLink($slug->slugify($mainMenu->getTitle()));

            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($mainMenu);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'validMessage',
                    "Le menu '".$mainMenu->getTitle()."' a bien été créé."
                );
                return $this->redirect($this->generateUrl('coud_app_adminmenu_managemainmenu'));

            } catch(\Doctrine\ORM\ORMException $e) {
                $this->get('session')->getFlashBag()->add(
                    'errorMessage',
                    "Une erreur est intervenue : ".$e
                );
            }

        }

    	$params['mainMenuForm'] = $mainMenuForm->createView();

    	return $this->render('admin/manageMenu/main_menu.html.twig', $params);
    }

    /**
     * @Route("/liste-sous-menu")
     *
     */
    public function listSubMenuAction() {
        $subMenus = $this->getDoctrine()->getRepository('CoudAppBundle:SubMenu')->findAll();

        return $this->render('admin/manageMenu/list_sub_menu.html.twig', array(
            'subMenus' => $subMenus
            ));

    }

    /**
     * @Route("/manage-sous-menu-principal")
     * @Method({"GET", "POST"})
     */
    public function manageSubMenuAction(Request $request) {
        $subMenu = new SubMenu();
        $subMenuForm = $this->createForm(SubMenuType::class, $subMenu);
        $subMenuForm->handleRequest($request);

        if($subMenuForm->isSubmitted() && $subMenuForm->isValid()) {
            $slug = $this->get('slugify');
            $subMenu->setLink($slug->slugify($subMenu->getTitle()));
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($subMenu);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'validMessage',
                    "Le sous-menu '".$subMenu->getTitle()."' a bien été enregistré"
                );
                return $this->redirect($this->generateUrl('coud_app_adminmenu_managesubmenu'));

            } catch(\Doctrine\ORM\ORMException $e) {
                $this->get('session')->getFlashBag()->add(
                    'errorMessage',
                    "Une erreur est intervenue : ".$e
                );
            }
        }

        return $this->render('admin/manageMenu/sub_menu.html.twig', array(
            'subMenuForm' =>  $subMenuForm->createView()
        ));
    }

    /**
     * @Route("/modifier-sous-menu/{id}")
     * @Method({"GET", "POST"})
     */
    public function modifySubMenuAction($id, Request $request) {
        $subMenu = $this->getDoctrine()->getRepository('CoudAppBundle:SubMenu')->findOneById($id);

        if(!$subMenu) {
            throw $this->createNotFoundException('Aucun sous-menu trouvé pour cet id : '.$id);
        }

        $menuForm = $this->createForm(SubMenuType::class, $subMenu);
        $menuForm->handleRequest($request);

        if($menuForm->isSubmitted() && $menuForm->isValid()) {
            $slug = $this->get('slugify');
            $subMenu->setLink($slug->slugify($subMenu->getTitle()));

            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($subMenu);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'validMessage',
                    "Le sous-menu '".$subMenu->getTitle()."' a bien été modifié"
                );
                return $this->redirect($this->generateUrl('coud_app_adminmenu_listsubmenu'));

            } catch(\Doctrine\ORM\ORMException $e) {
                $this->get('session')->getFlashBag()->add(
                    'errorMessage',
                    "Une erreur est intervenue : ".$e
                );
                return $this->redirect($this->generateUrl('coud_app_adminmenu_managesubmenu'));
            }

        }

        return $this->render('admin/manageMenu/modify_menu.html.twig', array(
            'menuForm' => $menuForm->createView()
            ));
    }


    /**
     * @Route("/modifier-menu-principal/{id}")
     * @Method({"GET", "POST"})
     */
    public function modifyMainMenuAction($id, Request $request) {
        $mainMenu = $this->getDoctrine()->getRepository('CoudAppBundle:MainMenu')->findSubMenuOfMainMenu($id);

        if(!$mainMenu) {
            throw $this->createNotFoundException('Aucun menu trouvé pour cet id : '.$id);
        }

        $menuForm = $this->createForm(MainMenuEditType::class, $mainMenu);
        $menuForm->handleRequest($request);

        if($menuForm->isSubmitted() && $menuForm->isValid()) {
            $slug = $this->get('slugify');
            $mainMenu->setLink($slug->slugify($mainMenu->getTitle()));

            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($mainMenu);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'validMessage',
                    "Le menu '".$mainMenu->getTitle()."' a bien été modifié"
                );
                return $this->redirect($this->generateUrl('coud_app_adminmenu_managemainmenu'));

            } catch(\Doctrine\ORM\ORMException $e) {
                $this->get('session')->getFlashBag()->add(
                    'errorMessage',
                    "Une erreur est intervenue : ".$e
                );
                return $this->redirect($this->generateUrl('coud_app_adminmenu_managemainmenu'));
            }

        }

        return $this->render('admin/manageMenu/modify_menu.html.twig', array(
            'menuForm' => $menuForm->createView()
            ));
    }

    /**
     * @Route("/desactiver/{id}")
     *
     */
    public function desactivateMainMenuAction($id) {

        $service = $this->get('main_menu_service');
        $message = $service->manageStatut($id);

        $this->get('session')->getFlashBag()->add(
                    $message['type'],
                    $message['message']
                );

        return $this->redirect($this->generateUrl('coud_app_adminmenu_managemainmenu'));
    }

    /**
     * @Route("/activer/{id}")
     *
     */
    public function activateMainMenuAction($id) {

        $service = $this->get('main_menu_service');
        $message = $service->manageStatut($id);

        $this->get('session')->getFlashBag()->add(
                    $message['type'],
                    $message['message']
                );

        return $this->redirect($this->generateUrl('coud_app_adminmenu_managemainmenu'));
    }

    public function getAllMenuAction() {
        $mainMenu = $this->getDoctrine()->getRepository('CoudAppBundle:MainMenu')->findAllMenuAndSub();
        return $this->render('admin/manageMenu/templates/table_main_menu.html.twig', array(
            'mainMenu' => $mainMenu
        ));

    }
}
