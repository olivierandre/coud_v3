<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

use Coud\AppBundle\Entity\Article;
use Coud\AppBundle\Form\Type\ArticleType;
use Coud\AppBundle\Service\DetectMobileService;

/**
 * @Route("/admin/document")
 * @Method({"GET"})
 */

class AdminDocumentController extends Controller
{
    /**
     * @Route("/gestion-des-documents")
     *
     */
    public function homeAction()
    {
    	$params = array();
        $typeArticles = $this->getDoctrine()->getRepository("CoudAppBundle:TypeArticle")->findAll();
        $params['typeArticles'] = $typeArticles;

        return $this->render('admin/document/index.html.twig', $params);
    }

    /**
     * @Route("/creation/{typeName}")
     * @Method({"GET", "POST"})
     */
    public function createDocumentAction($typeName, Request $request)
    {
        $typeArticle = $this->getDoctrine()->getRepository("CoudAppBundle:TypeArticle")->findOneByName($typeName);
        $article = new Article();

        $service = $this->get('document_service');
        $serviceResponse = $service->createDocument($typeArticle);

        $articleForm = $this->createForm(ArticleType::class, $article, array('options' => $serviceResponse['formParams']));
        $articleForm->handleRequest($request);

        if ($articleForm->isSubmitted() && $articleForm->isValid()) {
            try {
                $article->setTypeArticle($typeArticle);
                $article->setAuthor($this->getUser());
                $em = $this->getDoctrine()->getManager();
                $em->persist($article);
                $em->flush();

                return $this->redirect($this->generateUrl('coud_app_admindocument_previewdocument', array(
                    'type' => strtolower($typeArticle->getName()),
                    'id' => $article->getId()
                )));

            } catch(\Doctrine\ORM\ORMException $e) {
                $this->get('session')->getFlashBag()->add(
                    'errorMessage',
                    "Une erreur est intervenue : ".$e
                );
            }
        }

        return $this->render($serviceResponse['render'], array(
            'typeArticle' => $typeArticle,
            'articleForm' => $articleForm->createView(),
            'images' => $serviceResponse['images']
        ));
    }

    /**
     * @Route("/modifier/{id}")
     * @Method({"GET", "POST"})
     */
    public function modifyDocumentAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $article = $em->getRepository('CoudAppBundle:Article')->findOneById($id);
        $typeArticle = $article->getTypeArticle();

        if(!$article) {
            throw $this->createNotFoundException('Aucun document trouvé pour cet id : '.$id);
        }

        $service = $this->get('document_service');
        $serviceResponse = $service->modifyDocument($typeArticle, $article);

        $articleForm = $this->createForm(ArticleType::class, $article, array("options" => $serviceResponse['formParams']));
        $articleForm->handleRequest($request);

        if ($articleForm->isSubmitted() && $articleForm->isValid()) {
            try {
                $article->setAuthor($this->getUser());
                $article->setDateModified(new \DateTime());
                $em->persist($article);
                $em->flush();

                return $this->redirect($this->generateUrl('coud_app_admindocument_previewdocument', array(
                    'type' => strtolower($typeArticle->getName()),
                    'id' => $id
                )));

            } catch(\Doctrine\ORM\ORMException $e) {
                $this->get('session')->getFlashBag()->add(
                    'errorMessage',
                    "Une erreur est intervenue : ".$e
                );
            }

        }

        return $this->render($serviceResponse['render'], array(
            'articleForm' => $articleForm->createView(),
            'images' => $serviceResponse['images'],
            'typeArticle' => $typeArticle
            ));
    }


    /**
     * @Route("/articles-important")
     *
     */
    public function listImportantAction()
    {
        $params = array();
        $articles = $this->getDoctrine()->getRepository('CoudAppBundle:Article')->findAllAndActive();
        $params['articles'] = $articles;

        return $this->render('admin/document/important.html.twig', $params);
    }

    /**
     * @Route("/important/{id}")
     *
     */
    public function manageImportantAction($id)
    {
        $service = $this->get('document_service');
        $message = $service->manageImportant($id);

        $this->get('session')->getFlashBag()->add(
                     $message['type'],
                     $message['message']
                 );

        return $this->redirect($this->generateUrl('coud_app_admindocument_listimportant', array(
                    'id' => $id
                )));
    }

    /**
     * @Route("/tous-les-documents")
     *
     */
    public function allDocumentsAction()
    {
        $articles = $this->getDoctrine()->getRepository('CoudAppBundle:Article')->findAll();
        $params = array(
            'articles' => $articles
            );
        return $this->render('admin/document/all_articles.html.twig', $params);
    }

    /**
     * @Route("/activer-document/{type}/{id}")
     *
     */
    public function activeDocumentAction($type, $id)
    {
        $service = $this->get('document_service');
        $message = $service->manageStatut($id);

        $this->get('session')->getFlashBag()->add(
                     $message['type'],
                     $message['message']
                 );

        return $this->redirect($this->generateUrl('coud_app_admindocument_listdocuments', array(
            "type" => $type
        )));
    }


    /**
     * @Route("/liste/{type}")
     *
     */
    public function listDocumentsAction($type)
    {
        $typeArticle = $this->getDoctrine()->getRepository('CoudAppBundle:TypeArticle')->findByName($type);
        $articles = $this->getDoctrine()->getRepository('CoudAppBundle:Article')->findByTypeArticle($typeArticle);

        return $this->render('admin/document/list_articles.html.twig', array(
            'articles' => $articles,
            'type' => $type
        ));
    }

    /**
     * @Route("/previsualisation/{type}/{id}")
     *
     */
    public function previewDocumentAction($id, $type, Request $request, DetectMobileService $detectMobileService)
    {
        $article = $this->getDoctrine()->getRepository('CoudAppBundle:Article')->findDocumentActifById($id);

        if(!$article) {
            throw $this->createNotFoundException('Aucun document trouvé pour cet id : '.$id);
        }

        $isMobileOrTablet = $detectMobileService->isMobileOrTablet($request->server->get('HTTP_USER_AGENT'));

        return $this->render('admin/document/preview_document.html.twig', array(
            'article' => $article,
            'type' => $type,
            'isMobileOrTablet' => $isMobileOrTablet
        ));
    }

}
