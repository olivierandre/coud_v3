<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

use Coud\AppBundle\Entity\ChangePassword;
use Coud\AppBundle\Form\Type\ChangePasswordType;
use Coud\AppBundle\Entity\User;
use Coud\AppBundle\Form\Type\UserType;
use Coud\AppBundle\Entity\NewPassword;
use Coud\AppBundle\Form\Type\NewPasswordType;

/**
 * @Route("/admin/compte")
 * @Method({"GET"})
 */

class AdminUserController extends Controller
{
    /**
     * @Route("/info")
     * @Method({"GET", "PUT"})
     */
    public function accountUserAction(Request $request)
    {
        $user = $this->getUser();
        $userForm = $this->createForm(UserType::class, $user, array(
            'method' => 'PUT'
        ));
        $userForm->handleRequest($request);

        if($userForm->isSubmitted() && $userForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'validMessage',
                'Votre compte a été mis à jour'
            );
        }

        return $this->render('admin/user/account.html.twig', array(
            'userForm' => $userForm->createView()
        ));
    }

    /**
     * @Route("/change-mot-de-passe")
     * @Method({"GET", "POST"})
     */
    public function updatePasswordAction(Request $request) {
        $change = new ChangePassword();
        $user = $this->getUser();

        if(!$user) {
            throw $this->createNotFoundException('Utilisateur inconnu');
        }

        $changePasswordForm = $this->createForm(ChangePasswordType::class, $change);
        $changePasswordForm->handleRequest($request);

        if($changePasswordForm->isSubmitted() && $changePasswordForm->isValid()) {
        	$service = $this->get('user_service');
            $user = $service->updatePasswordService($user, $change->getNewPassword());

            $service->autoLoginService($user);

            //  Envoi du mail
            $serviceMail = $this->get('mail_service');
            $serviceMail->sendMailToUserService($user, "change");

            return $this->redirect($this->generateUrl('coud_app_admin_home'));

        }

        return $this->render('admin/user/update_password.html.twig', array(
            'changePasswordForm' => $changePasswordForm->createView()
        ));
    }

    /**
     * @Route("/listes-des-utilisateurs")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function listUsersAction()
    {
        $users = $this->getDoctrine()->getRepository('CoudAppBundle:User')->getUsersAndRoles();

        return $this->render('admin/user/list_users.html.twig', array(
            'users' => $users
        ));
    }

    /**
     * @Route("/creation-utilisateur")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method({"GET", "POST"})
     */
    public function createUserAction(Request $request)
    {
        $user = new User();
        $userForm = $this->createForm(UserType::class, $user, array("options" => array(
            'admin' => true,
            'create' => true,
        )));

        $userForm->handleRequest($request);

        if($userForm->isSubmitted() && $userForm->isValid()) {
            $service = $this->get('user_service');
            $service->updatePasswordService($user, $user->getPassword());

            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

            } catch(\Doctrine\ORM\ORMException $e) {
                $this->get('session')->getFlashBag()->add(
                    'errorMessage',
                    'Une erreur est intervenue : '.$e
                );
            }

            //  Envoi du mail
            $serviceMail = $this->get('mail_service');
            $serviceMail->sendMailToUserService($user, "create");

            return $this->redirect($this->generateUrl('coud_app_adminuser_listusers'));
        }

        return $this->render('admin/user/create_user.html.twig', array(
            'userForm' => $userForm->createView()
            ));
    }

    /**
     * @Route("/modifier-utilisateur/{id}")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method({"GET", "POST"})
     */
    public function modifyAccountUserByAdminAction($id, Request $request)
    {
        $user = new User();

        $user = $this->getDoctrine()->getRepository('CoudAppBundle:User')->findOneById($id);

        if(!$user) {
            throw $this->createNotFoundException('Aucun utilisateur trouvé pour cet id : '.$id);
        }

        $userForm = $this->createForm(UserType::class, $user, array("options" => array(
            'admin' => true,
            'create' => false
        )));
        $userForm->handleRequest($request);

        if($userForm->isSubmitted() && $userForm->isValid()) {

            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

            } catch(\Doctrine\ORM\ORMException $e) {
                $this->get('session')->getFlashBag()->add(
                    'errorMessage',
                    'Une erreur est intervenue : '.$e
                );
            }

            $this->get('session')->getFlashBag()->add(
                'validMessage',
                'Le compte de \''.$user->getFirstName().' '.$user->getLastName().'\' a été mis à jour'
            );
            return $this->redirect($this->generateUrl('coud_app_adminuser_listusers'));
        }

        return $this->render('admin/user/modify_user_by_admin.html.twig', array(
            'userForm' => $userForm->createView(),
            'user' => $user
        ));
    }

    /**
     * @Route("/activate-user/{id}")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method({"GET", "POST"})
     */
    public function activateUserAction($id)
    {
        $user = $this->getDoctrine()->getRepository('CoudAppBundle:User')->findOneById($id);

        if(!$user) {
            throw $this->createNotFoundException('Aucun document trouvé pour cet id : '.$id);
        }

        $user->setActive(!$user->isActive());

        try {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

        } catch(\Doctrine\ORM\ORMException $e) {
            $this->get('session')->getFlashBag()->add(
                'errorMessage',
                "Une erreur est intervenue : ".$e
            );
        }

        $message = "errorMessage";
        $active ="désactivé";

        if($user->isActive()) {
            $active = "activé";
            $message= "validMessage";
        }

        $this->get('session')->getFlashBag()->add(
            $message,
            'Le compte de \''.$user->getFirstName().' '.$user->getLastName().'\' a été '.$active.'.'
        );
        return $this->redirect($this->generateUrl('coud_app_adminuser_listusers'));

    }

    /**
     * @Route("/reinit-password-for-user/{id}")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method({"GET", "POST"})
     */
    public function reinitPasswordByAdminAction($id, Request $request)
    {
        $new = new NewPassword();

        $user = $this->getDoctrine()->getRepository('CoudAppBundle:User')->findOneById($id);

        if(!$user) {
            throw $this->createNotFoundException('Aucun document trouvé pour cet id : '.$id);
        }

        $newForm = $this->createForm(NewPasswordType::class, $new);
        $newForm->handleRequest($request);

        if($newForm->isSubmitted() && $newForm->isValid()) {
            $service = $this->get('user_service');
            $user = $service->updatePasswordService($user, $new->getPassword());

            //  Envoi du mail
            $serviceMail = $this->get('mail_service');
            $send = $serviceMail->sendMailToUserService($user, "reinit");

            if($send) {
                $this->get('session')->getFlashBag()->add(
                    'validMessage',
                    'Le mot de passe de '.$user->getFirstName().' '.$user->getLastName().' a été modifié. Un mail de confirmation a été envoyé à l\'utilisateur.'
                );
                return $this->redirect($this->generateUrl('coud_app_adminuser_listusers'));
            }

            $this->get('session')->getFlashBag()->add(
                'errorMessage',
                'Votre mot de passe a bien été modifié. Mais une erreur est intervenue et votre mail n\'a pas été transmis.'
            );
        }

        return $this->render('admin/user/change_password_by_admin.html.twig', array(
            'user' => $user,
            'newForm' => $newForm->createView()
        ));

    }

}
