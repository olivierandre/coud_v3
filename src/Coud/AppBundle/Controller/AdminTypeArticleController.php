<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

use Coud\AppBundle\Entity\TypeArticle;
use Coud\AppBundle\Form\Type\TypeArticleType;

/**
 * @Route("/admin/type-article")
 *  @Method({"GET"})
 */
class AdminTypeArticleController extends Controller
{
    /**
     * @Route("/")
     *
     */
    public function homeAction()
    {
        return $this->render('admin/type_article/index.html.twig', array());
    }

    /**
     * @Route("/liste")
     *
     */
    public function listTypeArticleAction()
    {
        $typeArticles = $this->getDoctrine()->getRepository("CoudAppBundle:TypeArticle")->findAll();

        return $this->render('admin/type_article/list.html.twig', array(
            "typeArticles" => $typeArticles
        ));
    }

    /**
     * @Route("/creation")
     * @Method({"GET", "POST"})
     */
    public function createTypeArticleAction(Request $request)
    {
        $typeArticle = new TypeArticle();
        $typeArticleForm = $this->createForm(TypeArticleType::class, $typeArticle);
        $typeArticleForm->handleRequest($request);

        if($typeArticleForm->isSubmitted() && $typeArticleForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typeArticle);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'validMessage',
                    "Nouveau type d'article créé"
                );
            return $this->redirect($this->generateUrl('coud_app_admintypearticle_createtypearticle'));
        }

        return $this->render('admin/type_article/add_type_article.html.twig', array(
            "typeArticleForm" => $typeArticleForm->createView(),
            "typeArticles" => $typeArticle
        ));
    }

    /**
     * @Route("/modifier/{id}")
     * @Method({"GET", "PUT"})
     */
    public function modifyTypeArticleAction($id, Request $request)
    {
        $typeArticle = $this->getDoctrine()->getRepository("CoudAppBundle:TypeArticle")->findOneById($id);
        $typeArticleForm = $this->createForm(TypeArticleType::class, $typeArticle, array(
            'method' => 'PUT'
        ));
        $typeArticleForm->handleRequest($request);

        if($typeArticleForm->isSubmitted() && $typeArticleForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typeArticle);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'validMessage',
                "MAJ type d'article effectuée"
            );
            return $this->redirect($this->generateUrl('coud_app_admintypearticle_listtypearticle'));
        }

        return $this->render('admin/type_article/add_type_article.html.twig', array(
            "typeArticleForm" => $typeArticleForm->createView(),
            "typeArticle" => $typeArticle
        ));
    }

}
