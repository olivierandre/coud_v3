<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Coud\AppBundle\Entity\Image;
use Coud\AppBundle\Form\Type\ImageType;
use Coud\AppBundle\Entity\ImageSize;
use Coud\AppBundle\Form\Type\ImageSizeType;

/**
 * @Route("/admin/image")
 * @Method({"GET"})
 */
class AdminFileController extends Controller
{
    /**
     * @Route("/")
     */
    public function homeAction()
    {
        return $this->render('admin/image/index_image.html.twig');
    }

    /**
     * @Route("/definir-taille-image")
     * @Method({"GET", "POST"})
     */
    public function defineImageSizeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $imageSize = new ImageSize();
        $formImageSize = $this->createForm(ImageSizeType::class, $imageSize);
        $formImageSize->handleRequest($request);

        if ($formImageSize->isSubmitted() && $formImageSize->isValid()) {
            try {
                $em->persist($imageSize);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                    'validMessage',
                    "La taille '".$imageSize->getTitle()."' a bien été créé."
                );

                return $this->redirect($this->generateUrl('coud_app_adminfile_defineimagesize'));
            } catch (\Doctrine\ORM\ORMException $e) {
                $this->get('session')->getFlashBag()->add(
                    'errorMessage',
                    'Une erreur est intervenue : '.$e
                );
            }
        }

        return $this->render('admin/image/image_size.html.twig', array(
            'formImageSize' => $formImageSize->createView(),
            'allSize' => $em->getRepository('CoudAppBundle:ImageSize')->findAll(),
        ));
    }

    /**
     * @Route("/supprimer-taille-image/{id}")
     * @Method({"POST"})
     */
    public function deleteImageSizeAction($id, Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $response = new JsonResponse();

            $imageSize = $this->getDoctrine()->getRepository('CoudAppBundle:ImageSize')->findOneById($id);

            if (!$imageSize) {
                return $response->setData(array(
                    'result' => 0,
                    'response' => 'Aucun type d\'image n\'a trouvé pour cet id : '.$id,
                ));
            }
            $em = $this->getDoctrine()->getEntityManager();
            $em->remove($imageSize);
            $em->flush();

            return $response->setData(array(
                    'result' => 1,
                    'response' => 'Ce type d\'image a bien été supprimé.',
                ));
        }
    }

    /**
     * @Route(
     *  "/insérer-image", options = { "utf8": true }
     * )
     * @Method({"GET", "POST"})
     */
    public function insertImageAction(Request $request)
    {
        $image = new Image();
        $formImage = $this->createForm(ImageType::class, $image);
        $formImage->handleRequest($request);

        if ($formImage->isSubmitted() && $formImage->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();

                $image->setRealPath($this->get('kernel')->getRootDir().'/../web');
                $em->persist($image);
                $em->flush();
                $this->addFlash(
                    'validMessage',
                    "L'image '".$image->getTitle()."' a bien été créé."
                );

                return $this->redirect($this->generateUrl('coud_app_adminfile_insertimage'));
            } catch (\Doctrine\ORM\ORMException $e) {
                $this->addFlash(
                    'errorMessage',
                    'Une erreur est intervenue : '.$e
                );
            }
        }

        return $this->render('admin/image/insert_image.html.twig', array(
            'formImage' => $formImage->createView(),
        ));
    }

    /**
     * @Route("/voir-images")
     */
    public function listImagesAction()
    {
        $images = $this->getDoctrine()->getRepository('CoudAppBundle:Image')->findAllWithArticles();
        $sizes = $this->getDoctrine()->getRepository('CoudAppBundle:ImageSize')->findAll();

        return $this->render('admin/image/all_images.html.twig', array(
            'images' => $images,
            'sizes' => $sizes,
        ));
    }

    /**
     * @Route("/suppression-image/{id}")
     * @Method({"GET", "POST"})
     */
    public function removeImageAction($id, Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $response = new JsonResponse();

            $image = $this->getDoctrine()->getRepository('CoudAppBundle:Image')->findOneById($id);

            if (!$image) {
                return $response->setData(array(
                    'result' => 0,
                    'response' => 'Aucune image trouvé pour cet id : '.$id,
                ));
            }

            if (count($image->getArticle()) == 0 && count($image->getImageArticle()) == 0) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->remove($image);
                $em->flush();

                return $response->setData(array(
                    'result' => 1,
                    'response' => 'Votre image a bien été supprimée.',
                ));
            } else {
                return $response->setData(array(
                    'result' => 0,
                    'response' => 'Cette image est lié à un document et ne peut être supprimé.',
                ));
            }
        } else {
            //  Renvoi sur la home
            return $this->redirect($this->generateUrl('coud_app_home_index'));
        }
    }
}
