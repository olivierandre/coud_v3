<?php

namespace Coud\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

use Coud\AppBundle\Entity\LostPassword;
use Coud\AppBundle\Form\Type\LostPasswordType;
use Coud\AppBundle\Entity\NewPassword;
use Coud\AppBundle\Form\Type\NewPasswordType;


/**
 * @Route("/admin")
 * @Method({"GET"})
 */
class SecurityController extends Controller
{
    /**
     * @Route("/login")
     * @Method({"GET", "POST"})
     */
    public function loginAction(Request $request)
    {
        $session = $request->getSession();
        $error = '';

        // get the login error if there is one
        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(Security::AUTHENTICATION_ERROR);
        } else if (null !== $session && $session->has(Security::AUTHENTICATION_ERROR)) {
            $error = $session->get(Security::AUTHENTICATION_ERROR);
            $session->remove(Security::AUTHENTICATION_ERROR);
        }
        
        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(Security::LAST_USERNAME);

        return $this->render(
            'security/login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error,
            )
        );
    }

    /**
     * @Route("/lost")
     * @Method({"GET", "POST"})
     */
    public function lostPasswordAction(Request $request) {
        $lostPassword = new LostPassword();

        $lostForm = $this->createForm(LostPasswordType::class, $lostPassword);
        $lostForm->handleRequest($request);

        if ($lostForm->isSubmitted() && $lostForm->isValid()) {
            $user = $this->getDoctrine()->getRepository('CoudAppBundle:User')->findOneByEmail($lostPassword->getEmail());

            if($user) {
                //  Envoi du mail
                $serviceMail = $this->get('mail_service');
                $serviceMail->sendMailToUserService($user, "lost");

                return $this->redirect($this->generateUrl('coud_app_security_login'));

            } else {
                $this->get('session')->getFlashBag()->add(
                    'errorMessage',
                    'Votre email est inexistant de notre base !!!'
                );
            }
        }
        return $this->render("security/lost_password.html.twig", array(
            'lostForm' => $lostForm->createView()
        ));
    }

    /**
     * @Route("/reinitialiser/{token}/{email}")
     * @Method({"GET", "POST"})
     */
    public function newPasswordAction($token, $email, Request $request) {
        $user = $this->getDoctrine()->getRepository('CoudAppBundle:User')->findOneByToken($token);

        if($user && $user->getEmail() === $email) {
            $new = new NewPassword();
            $newForm = $this->createForm(NewPasswordType::class, $new);
            $newForm->handleRequest($request);

            if($newForm->isSubmitted() && $newForm->isValid()) {
                $service = $this->get('user_service');
                $user = $service->updatePasswordService($user, $new->getPassword());
                $service->autoLoginService($user);
                //  Envoi du mail
                $serviceMail = $this->get('mail_service');
                $serviceMail->sendMailToUserService($user, "reinit");

                return $this->redirect($this->generateUrl('coud_app_admin_home'));
            }

        } else {
            $this->get('session')->getFlashBag()->add(
                    'errorMessage',
                    "Un problème est intervenue. Une modification est déjà intervenue sur votre compte ou votre email est incorrect."
                );
            return $this->redirect($this->generateUrl('coud_app_security_login'));
        }

        return $this->render("security/new_password.html.twig", array('newForm' => $newForm->createView()));
    }
}
