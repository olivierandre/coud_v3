<?php

	namespace Coud\AppBundle\Util;

	class StringHelper {

		public function getASCII() {
        $chaineFinale = array();
        $indiceNumberZero = 48;
        $indiceNumberNeuf = 57;

        for(;$indiceNumberZero <= $indiceNumberNeuf; $indiceNumberZero++) {
            $chaineFinale[] .= chr($indiceNumberZero);
        }

        $indiceLettreA = 65;
        $indiceLettreZ = 90;

        for(;$indiceLettreA <= $indiceLettreZ; $indiceLettreA++) {
            $chaineFinale[] .= chr($indiceLettreA);
        }

        $indiceLettrea = 97;
        $indiceLettrez = 122;

        for(;$indiceLettrea <= $indiceLettrez; $indiceLettrea++) {
            $chaineFinale[] .= chr($indiceLettrea);
        }

        //  Etoile
        $chaineFinale[] .= chr(42);
        //  Point
        $chaineFinale[] .= chr(46); 

        return $chaineFinale;
    }

    public function randomSalt($chaine, $taille = 50) {
        $chaineFinale = "";

        for($i = 0; $i < $taille; $i++) {
            shuffle($chaine);
            $chaineFinale .= $chaine[0];
        }
        return $chaineFinale;
    }

    public function getSaltToken() {
        return $this->randomSalt($this->getASCII());
    }


    //  http://www.weirdog.com/blog/php/supprimer-les-accents-des-caracteres-accentues.html
    public static function removeAccentsAndSpace($str, $charset, $space = false, $lower = false) {
        if(!$charset) {
            $charset = 'utf-8';
        }

        $str = htmlentities($str, ENT_NOQUOTES, $charset);
        
        $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
        if($space) {
            $str = str_replace(' ', '-', $str);
        }
        if($lower) {
            $str = strtolower($str);
        }
        
        return $str;
    }
}
