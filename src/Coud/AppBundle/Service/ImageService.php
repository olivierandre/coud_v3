<?php

namespace Coud\AppBundle\Service;

use Coud\AppBundle\Entity\Aside;

class ImageService {

    private $slugify;

    public function __construct($slugify)
    {
        $this->slugify = $slugify;
    }

    public function saveImageForAside(Aside $aside) {
        if (null !== $aside->file) {
            // faites ce que vous voulez pour générer un nom unique
            $slug = $this->slugify->slugify($aside->getTitle());
            $aside->setPath($slug.'.'.$aside->file->guessExtension());
            $aside->upload();
            return $aside;
        }

        return null;
        
    }

}

