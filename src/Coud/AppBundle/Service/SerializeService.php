<?php

namespace Coud\AppBundle\Service;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class SerializeService {

    public function __construct() {
        $this->encoder = new JsonEncoder();
        $this->normalizer = new GetSetMethodNormalizer();
        $this->serializer = new Serializer(array($this->normalizer), array($this->encoder));

    }

    public function serializeObject(array $toReject, $entity) {
        $this->normalizer->setIgnoredAttributes($toReject);
        return $this->serializer->serialize($entity, 'json');
    }
}
