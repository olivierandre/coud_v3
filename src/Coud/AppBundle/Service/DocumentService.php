<?php

namespace Coud\AppBundle\Service;

use Coud\AppBundle\Entity\Article;
use Coud\AppBundle\Entity\TypeArticle;

use Doctrine\ORM\EntityManagerInterface;

class DocumentService {

	private $em;

	public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function updateViewDocument(Article $article) {
        $article->setView($article->getView() + 1);

        try {
            $this->em->persist($article);
            $this->em->flush();
        } catch(\Doctrine\ORM\ORMException $e) {
            throw new \Exception($e);
        }
    }

    public function manageImportant($id) {
        $message = array();

        $article = $this->em->getRepository('CoudAppBundle:Article')->findOneById($id);

        if(!$article) {
            throw $this->createNotFoundException('Aucun document trouvé pour cet id : '.$id);
        }

        $article->setImportant(!$article->getImportant());

        if($article->getImportant()) {
            $important = "important";
        } else {
            $important = "normal";
        }

        try {
            $this->em->persist($article);
            $this->em->flush();

            $message['type'] = 'validMessage';
            $message['message'] = "Le document '".$article->getTitle()."' a été considéré comme $important";

        } catch(\Doctrine\ORM\ORMException $e) {
            $message['type'] = 'errorMessage';
            $message['message'] = "Une erreur est intervenue : ".$e;

        }

        return $message;
    }

	public function manageStatut($id) {
		$message = array();

        $article = $this->em->getRepository('CoudAppBundle:Article')->findOneById($id);

        if(!$article) {
            throw $this->createNotFoundException('Aucun document trouvé pour cet id : '.$id);
        }

        $article->setPublished(!$article->getPublished());

    	if($article->getPublished()) {
    		$published = "publié";
    	} else {
    		$published = 'désactivé';
    	}

        try {
            $this->em->persist($article);
            $this->em->flush();

            $message['type'] = 'validMessage';
    				$message['message'] = "Le document '".$article->getTitle()."' a bien été $published";


        } catch(\Doctrine\ORM\ORMException $e) {
        	$message['type'] = 'errorMessage';
    			$message['message'] = "Une erreur est intervenue : ".$e;

        }

        return $message;
	}

    public function createDocument(TypeArticle $typeArticle) {

        $nameTypeArticle = strtolower($typeArticle->getName());
        $params = array(
            "images" => null,
            "render" => 'admin/document/article.html.twig',
            "formParams" => array(
                "modify" => false,
                "imageSlug" => "theme",
                "portfolio" => true
                )
            );

        if($nameTypeArticle == "article") {
            $params['formParams']['document'] = 'document';
            $params['formParams']['portfolio'] = false;
        } else if ($nameTypeArticle == "portfolio" || $nameTypeArticle == "square") {
            $params['formParams']['document'] = 'article';
        }

        if($nameTypeArticle == "article") {
            $params['images'] = $this->doctrine->getRepository('CoudAppBundle:Image')->findImageBySlug('article');
        }

        return $params;
    }


    public function modifyDocument(TypeArticle $typeArticle, Article $article) {

        $nameTypeArticle = strtolower($typeArticle->getName());

        $params = array(
            "images" => null,
            "render" => 'admin/document/article.html.twig',
            "formParams" => array(
                "modify" => true,
                "imageSlug" => "theme",
                "imageTheme" => $article->getImage(),
                "portfolio" => true
                )
            );

        if($nameTypeArticle == "article") {
            $params['formParams']['document'] = 'document';
            $params['formParams']['portfolio'] = false;

        } else if ($nameTypeArticle == "portfolio" || $nameTypeArticle == "square") {
            $params['formParams']['document'] = 'article';
        }

        if($nameTypeArticle == "article") {
            $params['images'] = $this->doctrine->getRepository('CoudAppBundle:Image')->findImageBySlug('article');
        }

        return $params;


    }
}
