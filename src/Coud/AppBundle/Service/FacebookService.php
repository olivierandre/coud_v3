<?php

namespace Coud\AppBundle\Service;

use Facebook\Facebook;

class FacebookService
{
    const TIMEOUT = 3600;

    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function getFacebookFanCount()
    {
        if (!apcu_fetch('facebook_fan_count')) {
            apcu_delete('facebook_fan_count');
            $appId = $this->container->getParameter('facebook_app_id');
            $appSecret = $this->container->getParameter('facebook_app_secret');
            $facebookId = $this->container->getParameter('facebook_id');
            $query = '/'.$facebookId.'?fields=fan_count';
            $accessToken = $appId.'|'.$appSecret;
            $fb = new Facebook([
              'app_id' => $appId,
              'app_secret' => $appSecret,
              'default_graph_version' => $this->container->getParameter('facebook_app_version'),
            ]);

            $request = $fb->get($query, $accessToken);
            $object = $request->getGraphObject();
            $fanCount = $object->getProperty('fan_count');
            apcu_store('facebook_fan_count', $fanCount, self::TIMEOUT);
        }

        return apcu_fetch('facebook_fan_count');
    }
}
