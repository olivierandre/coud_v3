<?php

namespace Coud\AppBundle\Service;

use  Symfony\Component\HttpKernel\KernelInterface;

class JsonService {

    private $kernel;
    private $path;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
        $this->path = '@CoudAppBundle/Resources/doc/';
    }

    public function getJson($file) {
        $path = $this->kernel->locateResource($this->path.$file);
        $content = file_get_contents($path);
        $json = json_decode($content, true);
        unset($content);

        return $json;
    }

    public function saveJson($file, $data) {
        file_put_contents($this->kernel->locateResource($this->path.$file), json_encode($data));
        unset($file);
    }

    public function constructJson($json, $data) {
        foreach ($data as $key => $value) {
            $json[$key]['value'] = $value;
        }

        return $json;
    }

}
