<?php

namespace Coud\AppBundle\Service;

class ToastrService {

    public function setMessage($type = 'success', $message = 'Merci', $title = '')
    {
        return [
            'type' => $type,
            'message' => $message,
            'title' => $title
        ];
    }

}
