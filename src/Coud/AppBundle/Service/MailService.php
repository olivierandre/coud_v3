<?php

namespace Coud\AppBundle\Service;

use Coud\AppBundle\Entity\User;
use Coud\AppBundle\Entity\Comment;

class MailService
{
    private $mailer;

    public function __construct($mailer, $route, $templating, $session)
    {
        $this->mailer = $mailer;
        $this->route = $route;
        $this->templating = $templating;
        $this->session = $session;
    }

    private function setBodyOrPart($view, $user, $contentType, $token = null, $email = null)
    {
        if ($token === null && $email === null) {
            $this->templating->render($view, array(
                'road' => $this->route->generate('coud_app_security_newpassword', array(
                    'token' => $user->getToken(),
                    'email' => $user->getEmail(), ), true), ), 'text/html');
        }

        return $this->templating->render($view, array(
            'user' => $user, ), $contentType);
    }

    public function sendMailToUserService(User $user, $theme)
    {
        switch ($theme) {
            case 'contact':

                break;
            case 'change':
                $sub = 'Votre mot de passe a bien été modifié';
                // $body = $this->templating->renderView('CoudAppBundle:Email:change.txt.twig', array(
                //     'user' => $user, ), 'text/html');
                $body = setBodyOrPart('CoudAppBundle:Email:change.html.twig', $user, 'text/html');
                $part = setBodyOrPart('CoudAppBundle:Email:change.txt.twig', $user, 'text/plain');
                $validMessage = 'Votre mot de passe a été modifié. Un mail de confirmation vous a été envoyé.';
                $errorMessage = "Votre mot de passe a bien été modifié. Mais une erreur est intervenue et votre mail n'a pas été transmis.";
                break;
            case 'create':
                // $body = $this->templating->renderView('CoudAppBundle:Email:create_account.txt.twig', array(
                //     'user' => $user, ), 'text/html');
                $body = setBodyOrPart('CoudAppBundle:Email:create_account.html.twig', $user, 'text/html');
                $part = setBodyOrPart('CoudAppBundle:Email:create_account.txt.twig', $user, 'text/plain');
                $sub = 'Création compte';
                $validMessage = "Le compte de '".$user->getFirstName().' '.$user->getLastName()."' a été créé et un mail lui a été envoyé.";
                $errorMessage = "Votre compte a bien été créée. Mais une erreur est intervenue et votre mail n'a pas été transmis.";
                break;
            case 'lost':
                // $body = $this->templating->renderView('CoudAppBundle:Email:reinit.txt.twig', array(
                //     'road' => $this->route->generate('coud_app_security_newpassword', array(
                //         'token' => $user->getToken(),
                //         'email' => $user->getEmail())
                //     , true)), 'text/html');
                $body = setBodyOrPart('CoudAppBundle:Email:reinit.html.twig', $user, 'text/html', $user->getToken(), $user->getEmail());
                $part = setBodyOrPart('CoudAppBundle:Email:reinit.txt.twig', $user, 'text/plain', $user->getToken(), $user->getEmail());
                $sub = 'Mot de passe perdu';
                $validMessage = 'Un mail vous a été envoyé pour réinitialiser votre mot de passe';
                $errorMessage = "Une erreur est intervenue. Merci d'effectuer une nouvelle demande de réinitialisation.";
            break;
        }

        $subject = 'Coud la Comme Judith | '.$sub;

        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom('contact@coudlacommejudith.fr')
                ->setTo($user->getEmail())
                ->setContentType('text/html')
                ->setBody($body)
                ->addPart($part);

        $send = $this->mailer->send($message);

        if ($send) {
            $this->session->getFlashBag()->add(
                'validMessage',
                $validMessage
            );
        } else {
            $this->session->getFlashBag()->add(
                'errorMessage',
                $errorMessage
            );
        }
    }

    public function sendMailContact(Comment $contact)
    {
        $template = $this->templating->render('CoudAppBundle:Email:fromContact.html.twig', array('contact' => $contact), 'text/html');
        $part = $this->templating->render('CoudAppBundle:Email:fromContact.txt.twig', array('contact' => $contact), 'text/plain');
        $subject = 'Coud la Comme Judith | Contact';

        //  Mail pour Judith
        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($contact->getEmail())
                ->setTo('contact@coudlacommejudith.fr')
                ->setContentType('text/html')
                ->setBody($template)
                ->addPart($part);
        $send = $this->mailer->send($message);

        //  Mail pour le contact
        $template = $this->templating->render('CoudAppBundle:Email:toContact.html.twig', array('contact' => $contact), 'text/html');
        $part = $this->templating->render('CoudAppBundle:Email:toContact.txt.twig', array('contact' => $contact), 'text/plain');
        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom('contact@coudlacommejudith.fr')
                ->setTo($contact->getEmail())
                ->setContentType('text/html')
                ->setBody($template)
                ->addPart($part);
        $send = $this->mailer->send($message);

        return $send;
    }

    public function sendMailComment($title)
    {
        $subject = 'Coud la Comme Judith | '.$title;

        $template = $this->templating->render('CoudAppBundle:Email:comment.html.twig', array('title' => $title), 'text/html');
        $part = $this->templating->render('CoudAppBundle:Email:comment.txt.twig', array('title' => $title), 'text/plain');

        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom('contact@coudlacommejudith.fr')
                ->setTo('contact@coudlacommejudith.fr')
                ->setContentType('text/html')
                ->setBody($template)
                ->addPart($part);
        $send = $this->mailer->send($message);

        return $send;
    }
}
