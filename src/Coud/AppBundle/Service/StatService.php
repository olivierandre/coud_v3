<?php

namespace Coud\AppBundle\Service;

class StatService {

    public function createJsonForBasicColumn($tab, $title, $yAxisTitle) {

        return (array(
            'chart' => array(
                "type" => "column"
                ),
            'title' => array(
                "text" => $title
                ),
            'xAxis' => array(
                "categories" => $tab['categories'],
                "crosshair" => false
                ),
            'yAxis' => array(
                "min" => 0,
                "title" => array(
                    "text" => $yAxisTitle
                    )
                ),
            'plotOptions' => array(
                "column" => array(
                    "pointPadding" => 0.2,
                    "borderWidth" => 0
                    )
                ),
            'series' => $tab['series']
            ));

    }

}
