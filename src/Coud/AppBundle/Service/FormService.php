<?php

namespace Coud\AppBundle\Service;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\LessThan;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;

class FormService {

    private $factory;

    public function __construct($factory)
    {
        $this->factory = $factory;
    }

    public function buildForm($parameters) {
        $builder = $this->factory->createBuilder();

        foreach ($parameters as $key => $value) {
            $constraints = array();

            if(!empty($value['constraints'])) {
                $constraints =  $this->addConstraints($value['constraints']);
            }

            if($value['type'] == 'number') {
                $classe = NumberType::class;
            }

            $builder->add($key, new $classe(), array(
                    'label' => $value['title'],
                    'data' => $value['value'],
                    'constraints' => $constraints
                ));
        }

        $builder->add('submit', SubmitType::class, array(
                    'label' => 'Valider',
                    'attr' => array(
                        'class' => 'btn btn-primary'
                        )
                ));

        return $builder->getForm();

    }

    private function addConstraints($parameter) {
        $constraints = array();
        switch ($parameter) {
            case (!empty('notBlank')):
                array_push($constraints, new NotBlank(array(
                    'message' => "Ce champ ne peut être vide"
                    )));

            case(!empty('minValue')):
                $min = $parameter['minValue'];
                array_push($constraints, new GreaterThan(array(
                    'value' => $min,
                    'message' => "La valeur doit être supérieur à ".$min
                    )));

            case(!empty('maxValue')):
                $max = $parameter['maxValue'];
                array_push($constraints, new LessThan(array('value' => $max,
                    'message' => "La valeur doit être inférieur à ".$max
                    )));

        }

        return $constraints;
    }

    //  http://loiclefloch.fr/1034-gestion-des-erreurs-de-formulaire-avec-symfony2-et-ajax/
    public function getErrorsAsArray(Form $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error)
            $errors[] = $error->getMessage();

        foreach ($form->all() as $key => $child) {
            if($child instanceof Form) {
                if ($err = $this->getErrorsAsArray($child))
                    $errors[$key] = $err;
            }

        }
        return $errors;

    }

}
