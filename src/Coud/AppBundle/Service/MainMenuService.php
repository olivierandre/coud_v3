<?php

namespace Coud\AppBundle\Service;

class MainMenuService {

	private $doctrine;

	public function __construct($doctrine)
    {
        $this->doctrine = $doctrine;
    }

	public function manageStatut($id) {
		$message = array();

        $mainMenu = $this->doctrine->getRepository('CoudAppBundle:MainMenu')->findOneById($id);
        
        if(!$mainMenu) {
            throw $this->createNotFoundException('Aucun menu trouvé pour cet id : '.$id);
        }

    	$mainMenu->setActive(!$mainMenu->getActive());

    	if($mainMenu->getActive()) {
    		$active = "activé";
    	} else {
    		$active = 'désactivé';
    	}

        try {
            $em = $this->doctrine->getManager();
            $em->persist($mainMenu);
            $em->flush();

            $message['type'] = 'validMessage';
    		$message['message'] = "Le menu '".$mainMenu->getTitle()."' a bien été $active";
            

        } catch(\Doctrine\ORM\ORMException $e) {
        	$message['type'] = 'errorMessage';
    		$message['message'] = "Une erreur est intervenue : ".$e;
            
        }

        return $message;
	}
}
