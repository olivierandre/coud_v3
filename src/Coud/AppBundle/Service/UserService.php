<?php

namespace Coud\AppBundle\Service;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Coud\AppBundle\Util\StringHelper;
use Symfony\Component\HttpFoundation\RequestStack;

use Coud\AppBundle\Entity\User;

class UserService {

    private $encoderFactory;
    private $securityTokenStorage;
    private $eventDispatcher;
    private $requestStack;

    public function __construct($encoderFactory, $securityTokenStorage, $eventDispatcher, RequestStack $requestStack, $doctrine) {
        $this->encoderFactory = $encoderFactory;
        $this->securityTokenStorage = $securityTokenStorage;
        $this->eventDispatcher = $eventDispatcher;
        $this->requestStack = $requestStack;
        $this->doctrine = $doctrine;
    }

    public function updatePasswordService(User $user, $password) {
        $stringHelper = new StringHelper();
        // Gestion du salt
        $user->setSalt($stringHelper->getSaltToken());
        // Gestion du token
        $user->setToken($stringHelper->getSaltToken());

        // Crypté les passwords
        $factory = $this->encoderFactory;
        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword($password, $user->getSalt());

        //  Nouvelle version de cryptage => http://symfony.com/blog/new-in-symfony-2-6-security-component-improvements
        //  A tester

        $user->setPassword($password);
        try {
            $em = $this->doctrine->getManager();
            $em->persist($user);
            $em->flush();
        } catch(\Doctrine\ORM\ORMException $e) {
            $this->get('session')->getFlashBag()->add(
                'errorMessage',
                "Une erreur est intervenue : ".$e
            );
        }

        return $user;
    }

    public function autoLoginService(User $user) {
        // Auto-login (Attention au nom du firewall : dans ce cas, c'est secured_area cf: security.yml)
        $token = new UsernamePasswordToken($user, $user->getPassword(), "secured_area", $user->getRoles());

        $this->securityTokenStorage->setToken($token);
        // Fire the login event
        $event = new InteractiveLoginEvent($this->requestStack->getCurrentRequest(), $token);
        $this->eventDispatcher->dispatch("security.interactive_login", $event);
    }
}
