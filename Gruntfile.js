(function() {
    "use strict";

    module.exports = function(grunt) {
        // Chargement automatique de tous nos modules
        require('load-grunt-tasks')(grunt);

        // Time how long tasks take. Can help when optimizing build times
        require('time-grunt')(grunt);

        // Configuration des plugins
        grunt.initConfig({
            //	Permet d'initialiser les liens dans le fichier HTML
            wiredep: {
                app: {
                    src: ['app/Resources/views/layouts/base.html.twig'],
                    ignorePath: /^(\.\.\/)*web./,
                    exclude: [/highcharts-release/],
                    fileTypes: {
                        html: {
                            replace: {
                                js: '<script src="{{ asset(\'{{filePath}}\')}}"></script>',
                                css: '<link rel="stylesheet" href="{{ asset(\'{{filePath}}\')}}" />'
                            }
                        }
                    }
                },
                appAdmin: {
                    src: ['app/Resources/views/layouts/base_admin.html.twig'],
                    ignorePath: /^(\.\.\/)*web./,
                    fileTypes: {
                        html: {
                            replace: {
                                js: '<script src="{{ asset(\'{{filePath}}\')}}"></script>',
                                css: '<link rel="stylesheet" href="{{ asset(\'{{filePath}}\')}}" />'
                            }
                        }
                    }
                }

            },
            compass: {
                sass: {
                    options: {
                        sassDir: 'web/css/scss',
                        cssDir: 'web/.tmp',
                        importPath: 'web/css/scss/import',
                        outputStyle: 'compressed',
                        noLineComments: true
                    }
                }
            },
            stylus: {

                compile: {
                    options: {
                        urlfunc: 'embedurl',
                        compress: true
                    },
                    files: {
                        //'web/.tmp/responsive.css': 'web/css/stylus/**/responsive.styl',
                        //'web/.tmp/main.css': 'web/css/stylus/**/main.styl',
                        'web/css/global.min.css': 'web/css/stylus/**/global.styl',
                        'web/css/homepage.min.css': 'web/css/stylus/**/homepage.styl',
                        'web/css/article.min.css': 'web/css/stylus/**/article.styl',
                        'web/css/navbar.min.css': 'web/css/stylus/**/navbar.styl',
                        'web/css/footer.min.css': 'web/css/stylus/**/footer.styl',
                        //'web/.tmp/admin.css': 'web/css/stylus/**/admin.styl'
                        //'web/.tmp/admin.css': 'web/css/stylus/**/admin.styl'

                    }
                }


            },
            coffee: {
                compile: {
                    files: {
                        //'web/js/app_admin_image.js': 'web/js/coffee/**/app_admin_image.coffee',
                        'web/.tmp/js/admin/app_admin.js': 'web/js/coffee/**/app_admin.coffee',
                        //'web/js/app.js': 'web/js/coffee/**/app.coffee',
                    }
                }
            },
            // Empties folders to start fresh
            clean: {
                dist: {
                    files: [{
                        dot: true,
                        src: [
                            'web/.tmp',
                        ]
                    }]
                },
                server: '.tmp'
            },
            concat: {
                options: {
                    separator: ';'
                },
                dist: {
                    src: [
                        // 'web/.tmp/style.min.css',
                        // 'web/.tmp/main.css',
                        // 'web/.tmp/responsive.css',
                        // 'web/bundles/lib/animate.css/animate.min.css',
                        // 'web/bundles/lib/Hover/css/hover-min.css',
                        // 'web/bundles/lib/toastr/toastr.min.css',
                        // 'web/.tmp/owl.min.css'
                        'web/.tmp/global.css'

                    ],
                    dest: 'web/css/global.min.css'

                },
                // css_bundle: {
                //     src: [
                //         'web/bundles/lib/animate.css/animate.min.css',
                //         'web/bundles/lib/Hover/css/hover-min.css',
                //         'web/bundles/lib/toastr/toastr.min.css',
                //         'web/.tmp/owl.min.css'
                //     ],
                //     dest: 'web/css/bundles.min.css'
                // },
                // admin: {
                //     src: ['web/.tmp/admin.css'],
                //     dest: 'web/css/admin.min.css'
                // },
                // js: {
                //     src: ['web/.tmp/js/*.js'],
                //     dest: 'web/js/app.min.js'
                // },
                jsAdmin: {
                    src: ['web/.tmp/js/admin/*.min.js'],
                    dest: 'web/js/app_admin.min.js'
                },
                jsAdminImage: {
                    src: ['web/.tmp/js/admin/image/*.min.js'],
                    dest: 'web/js/app_admin_image.min.js'
                },
                jsFacebook: {
                    src: ['web/javascripts/facebook.js'],
                    dest: 'web/js/facebook.min.js'
                },
                scripts: {
                    src: [
                        'web/.tmp/js/jquery.min.js',
                        'web/.tmp/js/bootstrap.min.js',
                        'web/.tmp/js/toastr.min.js',
                        'web/.tmp/js/owl.carousel.min.js',
                        'web/.tmp/js/*.js'
                    ],
                    dest: 'web/js/scripts.min.js'
                }
            },
            postcss: {
                options: {
                    map: false,
                    processors: [
                        require('autoprefixer')({
                            browsers: ['last 2 versions']
                        })
                    ]
                },
                dist: {
                    src: 'web/.tmp/*.css'
                }
            },
            cssmin: {
                options: {
                    keepSpecialComments: 0
                },
                target: {
                    // files: [{
                    //     expand: true,
                    //     cwd: 'web/.tmp/',
                    //     //src: ['style.css', '!*.min.css'],
                    //     src: ['web/bundles/lib/owl/assets/*.css'],
                    //     dest: 'web/.tmp/',
                    //     ext: '.min.css'
                    // }]
                    files: {
                      'web/.tmp/owl.min.css': ['web/bundles/lib/owl/assets/owl.carousel.css', 'web/bundles/lib/owl/assets/owl.theme.carousel.css']
                    }
                }
            },
            uglify: {
                options: {
                    mangle: false
                },
                target: {
                    files: {
                        //'web/.tmp/js/app.min.js': 'web/js/.tmp/app.js',
                        'web/.tmp/js/admin/app_admin.min.js': 'web/.tmp/js/admin/app_admin.js',
                        'web/.tmp/js/admin/image/app_admin_image.min.js': 'web/.tmp/js/web/javascripts/app_admin_image.js',
                        'web/.tmp/js/_slider.min.js': 'web/.tmp/js/web/javascripts/slider.js',
                        'web/.tmp/js/photo.min.js': 'web/.tmp/js/web/javascripts/app.js',
                        'web/.tmp/js/square.min.js': 'web/.tmp/js/web/javascripts/square.js',
                        'web/.tmp/js/sizeImage.min.js': 'web/.tmp/js/web/javascripts/sizeImage.js',
                        'web/js/socialNetwork.min.js': 'web/javascripts/socialNetwork.js',
                        'web/.tmp/js/jquery.min.js': 'web/bundles/lib/jquery/dist/jquery.min.js',
                        'web/.tmp/js/bootstrap.min.js': 'web/bundles/lib/bootstrap-sass/assets/javascripts/bootstrap.min.js',
                        'web/.tmp/js/toastr.min.js': 'web/bundles/lib/toastr/toastr.min.js',
                        'web/.tmp/js/owl.carousel.min.js': 'web/bundles/lib/owl/owl.carousel.min.js'
                    }
                }
            },

            copy: {
                main: {
                    files: [{
                        expand: true,
                        src: ['web/javascripts/*'],
                        dest: 'web/.tmp/js'
                    }, ],
                },
            },

            watch: {
                css: {
                    files: [
                        'web/css/scss/**/*.scss'
                    ],
                    tasks: ['css']
                },
                stylus: {
                    files: [
                        'web/css/stylus/**/*.styl'
                    ],
                    tasks: ['compass', 'stylus', 'cssmin', 'concat', 'clean:dist']
                },
                bower: {
                    files: ['bower.json'],
                    tasks: ['wiredep']
                },
                gruntfile: {
                    files: ['Gruntfile.js']
                },
                coffee: {
                    files: ['web/js/coffee/**/*.coffee'],
                    tasks: ['coffee']
                },
                js: {
                    files: ['web/javascripts/**/*.js'],
                    tasks: ['copy', 'uglify', 'concat']
                }

            }

        });

        // Déclaration des différentes tâches
        grunt.registerTask('default', [
            'wiredep',
            'compass',
            'stylus',
            'postcss',
            'coffee',
            'copy',
            'cssmin',
            'uglify',
            'concat',
            'clean:dist',
        ]);

        grunt.registerTask('serve', 'Compile then start a connect web server', function(target) {
            if (target === 'dist') {
                return grunt.task.run(['build', 'connect:dist:keepalive']);
            }

            grunt.task.run([
                //'clean:server',
                //'wiredep',
                //'concurrent:server',
                //'autoprefixer',
                'connect:livereload',
                'watch'
            ]);
        });

        grunt.registerTask('build', []);

        grunt.registerTask('css', ['compass']);
    };
}());
